<?php
include('../components/header.php');
$label = 'Agregar';
$src = null;
if (isset($_GET['action']) && $_GET['action'] == 'edit') {
    $label = 'Modificar';
    if (isset($_GET['id'])) {
        $mBanner = new Banner();
        $banner = $mBanner->getBannerbyId($_GET['id']);
        $src = '/assets/img/slider/banner/'.$banner['bannerSrc'];
    }
}
?>
<div class="container is-view">
    <div class="columns">
        <div class="column">
            <h1 class="is-size-4"><?= $label ?> Banner</h1>
            <nav class="breadcrumb" aria-label="breadcrumbs">
                <ul>
                    <li><a href="/in/banners">Banners</a></li>
                    <li class="is-active"><a aria-current="page"><?= $label ?> Banner</a></li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="columns">
        <div class="column is-two-thirds">
            <form action="#" id="frm-banner">
                <fieldset class="f-md">
                    <label for="">Imagen (1100 x 560)</label>
                    <input type="file" name="bannerSrc" id="bannerSrc" accept="image/*">
                    <input type="hidden" id="bannerSrc-name" name="bannerSrc-name" value="<?= (isset($banner['bannerSrc']) ? $banner['bannerSrc'] : '') ?>">
                </fieldset>
                <fieldset class="f-md">
                    <label for="">Ciudad</label>
                    <select name="bannerCity" id="bannerCity">
                        <option value="">General</option>
                        <?php
                        $cityUriExtend = '';
                        if (isset($banner['bannerCity'])) {
                            $cityUriExtend = $banner['bannerCity'];
                        }
                        foreach ($cities as $key => $city) {
                            $selected = '';
                            if ($key == $cityUriExtend) {
                                $selected = 'selected';
                            }
                            echo '<option value="' . $city->url . '" ' . $selected . ' >' . $city->text . '</option>' . "\r\n";
                        }
                        ?>
                    </select>
                    <input type="hidden" id="last-city" name="last-city" value="<?= $cityUriExtend ?>">
                </fieldset>
                <div class="clr"></div>
                <fieldset class="f-md">
                    <label for="">Title</label>
                    <input type="text" id="bannerTitle" name="bannerTitle" placeholder="Title" value="<?= (isset($banner['bannerTitle']) ? $banner['bannerTitle'] : '') ?>">
                </fieldset>
                <fieldset class="f-md">
                    <label for="">Alt</label>
                    <input type="text" id="bannerAlt" name="bannerAlt" placeholder="Alt" value="<?= (isset($banner['bannerAlt']) ? $banner['bannerAlt'] : '') ?>">
                </fieldset>
                <div class="clr"></div>
                <fieldset>
                    <input type="hidden" name="idbanner" id="idbanner" value="<?= (isset($banner['idbanner']) ? $banner['idbanner'] : '') ?>">
                    <input type="hidden" name="action" id="action" value="<?= ((isset($_GET['action']) && $_GET['action'] == 'edit') ? $_GET['action'] : 'create') ?>">
                    <input type="submit" value="<?= $label ?>">
                </fieldset>
            </form>
        </div>
        <div class="column">
            <img id="is-preview" class="is-img-full" src="<?= $src ?>" alt="">
        </div>
    </div>
</div>
<?php include('../components/footer.php'); ?>