<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view is-view-packages">
    <div class="container">
        <div class="columns is-multiline">

            <div class="column is-full">
                <h1 class="is-title-home">Paquetes Total<span class="cl-p">p</span><span class="cl-l">l</span><span class="cl-a">a</span><span class="cl-y">y</span></h1>

                <div class="is-custom-switch is-clearfix">
                    <h2 data-val="is-triple-pack" data-text="is-text-tripleplay" class="is-selected button-pkg">Tripleplay</h2>
                    <small><i class="fas fa-chevron-left"></i><i class="fas fa-chevron-right"></i></small>
                    <h2 data-val="is-doble-pack" data-text="is-text-dobleplay" class="button-pkg">Dobleplay</h2>
                </div>

                <p class="is-text is-text-packages is-text-tripleplay is-show" id="is-text-tripleplay">Los siguientes planes incluyen un decodificador HD con punto de acceso WIFI1 que te permitirá acceder a tus aplicaciones favoritas</p>
                <p class="is-text is-text-packages is-text-dobleplay" id="is-text-dobleplay">A partir del plan Emociónate+, disfruta por 30 días de un decodificador 4K con 275 canales y un punto de acceso que te permitirá usar tus aplicaciones favoritas.</p>

            </div>

            <div class="column is-full is-packages">
                <div class="columns is-multiline">

                    <!--TRIPLEPLAY-->

                    <?php foreach ($packList['tripleplay'] as $tripleplay) { ?>
                        <div class="column is-one-quarter is-triple-pack">
                            <div class="is-item is-default <?= $tripleplay['color'] ?>">
                                <div class="is-title">
                                    <strong><?= $tripleplay['nombre'] ?></strong>
                                    <span class="is-megas is-size-4">Internet de <?= $tripleplay['megas'] ?><small>Megas</small></span>
                                </div>
                                <div class="is-complements is-clearfix is-flex-div">
                                    <span class="<?= ($tripleplay['canales'] == 0) ? 'is-hidden' : '' ?>">
                                        <i class="fas fa-tv"></i>
                                        <small><strong><?= $tripleplay['canales'] ?></strong> Canales</small>
                                        <small class="is-tag"><?= $tripleplay['tvs'] ?> TV</small>
                                    </span>
                                    <span class="<?= ($tripleplay['lineas'] == 0) ? 'is-hidden' : '' ?>">
                                        <i class="fas fa-fax"></i>
                                        <small><strong><?= $tripleplay['lineas'] ?></strong> Línea(s) telefónica(s)</small>
                                    </span>
                                </div>
                                <div class="is-discount">
                                    <strong class="is-size-4"><small>$</small><?= $tripleplay['descuentomxn'] ?><small class="is-size-7 dpy-inl">MXN</small><span class="dpy-inl is-size-6"> de Descuento</span></strong>
                                    <span class="is-size-6">a partir del <?= $tripleplay['mesdescuento'] ?>° MES</span>
                                </div>
                                <div class="is-total">
                                    <span>Pronto Pago</span>
                                    <strong>$<?= number_format($tripleplay['prontopago'], 0, '.', ',') ?><small class="dpy-inl">MXN</small></strong>
                                    <span>Precio de Lista</span>
                                    <strong>$<?= number_format($tripleplay['listapago'], 0, '.', ',') ?><small class="dpy-inl">MXN</small></strong>
                                    <a href="/contrata?paquete=<?= $tripleplay['cve'] ?>" class="btn-paquete">Contratar</a>
                                    <small>*Consulta términos y condiciones</small>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                    <!--<div class="column is-one-quarter is-triple-pack">
                        <div class="is-item is-default">
                            <div class="is-title">
                                <strong>Paquete Sorprendete +</strong>
                                <span class="is-megas">Internet de 500<small>Megas</small></span>
                            </div>
                            <div class="is-complements is-clearfix">
                                <span>
                                    <i class="fas fa-tv"></i>
                                    <small><strong>80</strong> Canales</small>
                                    <small class="is-tag">2 TV</small>
                                </span>
                                <span>
                                    <i class="fas fa-fax"></i>
                                    <small><strong>1</strong> Línea(s) telefónica(s)</small>
                                </span>
                            </div>
                            <div class="is-discount">
                                <strong class="is-size-3"><small>$</small>80<small class="is-size-7">MXN</small></strong><strong> de Descuento</strong>
                                <span>a partir del 6° MES</span>
                            </div>
                            <div class="is-total">
                                <span>Pronto Pago</span>
                                <strong>$1,759<small class="dpy-inl">MXN</small></strong>
                                <span>Precio de Lista</span>
                                <strong>$1,839<small class="dpy-inl">MXN</small></strong>
                                <small>Consulta términos y condiciones*</small>
                            </div>
                        </div>
                    </div>-->

                    <!--<div class="column is-one-quarter is-triple-pack">
                        <div class="is-item is-default">
                            <div class="is-title">
                                <strong>Sorprendete <small>+</small></strong>
                                <span class="is-megas">500<small>Megas</small></span>
                            </div>
                            <div class="is-complements is-clearfix">
                                <span>
                                    <i class="fas fa-tv"></i>
                                    <small>80 Canales</small>
                                    <small class="is-tag">2 TV</small>
                                </span>
                                <span>
                                    <i class="fas fa-fax"></i>
                                    <small>1 Línea</small>
                                </span>
                            </div>
                            <div class="is-discount">
                                <strong>$80 Descuento</strong>
                                <span>de por vida a partir del mes 6</span>
                            </div>
                            <div class="is-total">
                                <span>Pronto Pago</span>
                                <strong>$1,759</strong>
                                <span>Precio de Lista</span>
                                <strong>$1,839</strong>
                                <small>Consulta términos y condiciones*</small>
                            </div>
                        </div>
                    </div>

                    <div class="column is-one-quarter is-triple-pack">
                        <div class="is-item is-default">
                            <div class="is-title">
                                <strong>Sorprendete</strong>
                                <span class="is-megas">350<small>Megas</small></span>
                            </div>
                            <div class="is-complements is-clearfix">
                                <span>
                                    <i class="fas fa-tv"></i>
                                    <small>80 Canales</small>
                                    <small class="is-tag">2 TV</small>
                                </span>
                                <span>
                                    <i class="fas fa-fax"></i>
                                    <small>1 Línea</small>
                                </span>
                            </div>
                            <div class="is-discount">
                                <strong>$70 Descuento</strong>
                                <span>de por vida a partir del mes 6</span>
                            </div>
                            <div class="is-total">
                                <span>Pronto Pago</span>
                                <strong>$1,559</strong>
                                <span>Precio de Lista</span>
                                <strong>$1,639</strong>
                                <small>Consulta términos y condiciones*</small>
                            </div>
                        </div>
                    </div>

                    <div class="column is-one-quarter is-triple-pack">
                        <div class="is-item is-blue">
                            <div class="is-title">
                                <strong>Emocionate <small>+</small></strong>
                                <span class="is-megas">250<small>Megas</small></span>
                            </div>
                            <div class="is-complements is-clearfix">
                                <span>
                                    <i class="fas fa-tv"></i>
                                    <small>80 Canales</small>
                                    <small class="is-tag">2 TV</small>
                                </span>
                                <span>
                                    <i class="fas fa-fax"></i>
                                    <small>1 Línea</small>
                                </span>
                            </div>
                            <div class="is-discount">
                                <strong>$60 Descuento</strong>
                                <span>de por vida a partir del mes 6</span>
                            </div>
                            <div class="is-total">
                                <span>Pronto Pago</span>
                                <strong>$1,309</strong>
                                <span>Precio de Lista</span>
                                <strong>$1,389</strong>
                                <small>Consulta términos y condiciones*</small>
                            </div>
                        </div>
                    </div>

                    <div class="column is-one-quarter is-triple-pack">
                        <div class="is-item is-blue">
                            <div class="is-title">
                                <strong>Emocionate</strong>
                                <span class="is-megas">150<small>Megas</small></span>
                            </div>
                            <div class="is-complements is-clearfix">
                                <span>
                                    <i class="fas fa-tv"></i>
                                    <small>80 Canales</small>
                                    <small class="is-tag">1 TV</small>
                                </span>
                                <span>
                                    <i class="fas fa-fax"></i>
                                    <small>1 Línea</small>
                                </span>
                            </div>
                            <div class="is-discount">
                                <strong>$60 Descuento</strong>
                                <span>de por vida a partir del mes 6</span>
                            </div>
                            <div class="is-total">
                                <span>Pronto Pago</span>
                                <strong>$999</strong>
                                <span>Precio de Lista</span>
                                <strong>$1,079</strong>
                                <small>Consulta términos y condiciones*</small>
                            </div>
                        </div>
                    </div>

                    <div class="column is-one-quarter is-triple-pack">
                        <div class="is-item is-pink">
                            <div class="is-title">
                                <strong>Diviértete <small>+</small></strong>
                                <span class="is-megas">80<small>Megas</small></span>
                            </div>
                            <div class="is-complements is-clearfix">
                                <span>
                                    <i class="fas fa-tv"></i>
                                    <small>80 Canales</small>
                                    <small class="is-tag">2 TV</small>
                                </span>
                                <span>
                                    <i class="fas fa-fax"></i>
                                    <small>1 Línea</small>
                                </span>
                            </div>
                            <div class="is-discount">
                                <strong>$50 Descuento</strong>
                                <span>de por vida a partir del mes 6</span>
                            </div>
                            <div class="is-total">
                                <span>Pronto Pago</span>
                                <strong>$759</strong>
                                <span>Precio de Lista</span>
                                <strong>$809</strong>
                                <small>Consulta términos y condiciones*</small>
                            </div>
                        </div>
                    </div>

                    <div class="column is-one-quarter is-triple-pack">
                        <div class="is-item is-pink">
                            <div class="is-title">
                                <strong>Diviértete</strong>
                                <span class="is-megas">40<small>Megas</small></span>
                            </div>
                            <div class="is-complements is-clearfix">
                                <span>
                                    <i class="fas fa-tv"></i>
                                    <small>80 Canales</small>
                                    <small class="is-tag">1 TV</small>
                                </span>
                                <span>
                                    <i class="fas fa-fax"></i>
                                    <small>1 Línea</small>
                                </span>
                            </div>
                            <div class="is-discount">
                                <strong>$40 Descuento</strong>
                                <span>de por vida a partir del mes 6</span>
                            </div>
                            <div class="is-total">
                                <span>Pronto Pago</span>
                                <strong>$659</strong>
                                <span>Precio de Lista</span>
                                <strong>$709</strong>
                                <small>Consulta términos y condiciones*</small>
                            </div>
                        </div>
                    </div>-->

                    <!--DOBLEPLAY-->
                    <?php foreach ($packList['dobleplay'] as $dobleplay) { ?>
                        <div class="column is-one-quarter is-doble-pack">
                            <div class="is-item is-default <?= $dobleplay['color'] ?>">
                                <div class="is-title">
                                    <strong><?= $dobleplay['nombre'] ?></strong>
                                    <span class="is-megas is-size-4">Internet de <?= $dobleplay['megas'] ?><small>Megas</small></span>
                                </div>
                                <div class="is-complements is-clearfix is-flex-div">
                                    <span class="<?= ($dobleplay['canales'] == 0) ? 'is-hidden' : '' ?>">
                                        <i class="fas fa-tv"></i>
                                        <small><strong><?= $dobleplay['canales'] ?></strong> Canales</small>
                                        <small class="is-tag"><?= $dobleplay['tvs'] ?> TV</small>
                                    </span>
                                    <span class="<?= ($dobleplay['lineas'] == 0) ? 'is-hidden' : '' ?>">
                                        <i class="fas fa-fax"></i>
                                        <small><strong><?= $dobleplay['lineas'] ?></strong> Línea(s) telefónica(s)</small>
                                    </span>
                                </div>
                                <div class="is-discount">
                                    <strong class="is-size-4"><small>$</small><?= $dobleplay['descuentomxn'] ?><small class="is-size-7 dpy-inl">MXN</small><span class="dpy-inl is-size-6"> de Descuento</span></strong>
                                    <span class="is-size-6">a partir del <?= $dobleplay['mesdescuento'] ?>° MES</span>
                                </div>
                                <div class="is-total">
                                    <span>Pronto Pago</span>
                                    <strong>$<?= number_format($dobleplay['prontopago'], 0, '.', ',') ?><small class="dpy-inl">MXN</small></strong>
                                    <span>Precio de Lista</span>
                                    <strong>$<?= number_format($dobleplay['listapago'], 0, '.', ',') ?><small class="dpy-inl">MXN</small></strong>
                                    <a href="/contrata?paquete=<?= $dobleplay['cve'] ?>" class="btn-paquete">Contratar</a>
                                    <small>*Consulta términos y condiciones</small>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <!--<div class="column is-one-quarter is-doble-pack">
                        <div class="is-item is-default">
                            <div class="is-title">
                                <strong>Sorprendete <small>+</small></strong>
                                <span class="is-megas">500<small>Megas</small></span>
                            </div>
                            <div class="is-complements is-clearfix">
                                <span style="width:100%;">
                                    <i class="fas fa-fax"></i>
                                    <small>1 Línea</small>
                                </span>
                            </div>
                            <div class="is-discount">
                                <strong>$80 Descuento</strong>
                                <span>de por vida a partir del mes 6</span>
                            </div>
                            <div class="is-total">
                                <span>Pronto Pago</span>
                                <strong>$1,549</strong>
                                <span>Precio de Lista</span>
                                <strong>$1,629</strong>
                                <small>Consulta términos y condiciones*</small>
                            </div>
                        </div>
                    </div>

                    <div class="column is-one-quarter is-doble-pack">
                        <div class="is-item is-default">
                            <div class="is-title">
                                <strong>Sorprendete</strong>
                                <span class="is-megas">350<small>Megas</small></span>
                            </div>
                            <div class="is-complements is-clearfix">
                                <span style="width:100%;">
                                    <i class="fas fa-fax"></i>
                                    <small>1 Línea</small>
                                </span>
                            </div>
                            <div class="is-discount">
                                <strong>$70 Descuento</strong>
                                <span>de por vida a partir del mes 6</span>
                            </div>
                            <div class="is-total">
                                <span>Pronto Pago</span>
                                <strong>$1,349</strong>
                                <span>Precio de Lista</span>
                                <strong>$1,429</strong>
                                <small>Consulta términos y condiciones*</small>
                            </div>
                        </div>
                    </div>

                    <div class="column is-one-quarter is-doble-pack">
                        <div class="is-item is-blue">
                            <div class="is-title">
                                <strong>Emocionate <small>+</small></strong>
                                <span class="is-megas">250<small>Megas</small></span>
                            </div>
                            <div class="is-complements is-clearfix">
                                <span style="width:100%;">
                                    <i class="fas fa-fax"></i>
                                    <small>1 Línea</small>
                                </span>
                            </div>
                            <div class="is-discount">
                                <strong>$60 Descuento</strong>
                                <span>de por vida a partir del mes 6</span>
                            </div>
                            <div class="is-total">
                                <span>Pronto Pago</span>
                                <strong>$1,099</strong>
                                <span>Precio de Lista</span>
                                <strong>$1,179</strong>
                                <small>Consulta términos y condiciones*</small>
                            </div>
                        </div>
                    </div>

                    <div class="column is-one-quarter is-doble-pack">
                        <div class="is-item is-blue">
                            <div class="is-title">
                                <strong>Emocionate</strong>
                                <span class="is-megas">150<small>Megas</small></span>
                            </div>
                            <div class="is-complements is-clearfix">
                                <span style="width:100%;">
                                    <i class="fas fa-fax"></i>
                                    <small>1 Línea</small>
                                </span>
                            </div>
                            <div class="is-discount">
                                <strong>$60 Descuento</strong>
                                <span>de por vida a partir del mes 6</span>
                            </div>
                            <div class="is-total">
                                <span>Pronto Pago</span>
                                <strong>$849</strong>
                                <span>Precio de Lista</span>
                                <strong>$929</strong>
                                <small>Consulta términos y condiciones*</small>
                            </div>
                        </div>
                    </div>

                    <div class="column is-one-quarter is-doble-pack">
                        <div class="is-item is-pink">
                            <div class="is-title">
                                <strong>Diviértete <small>+</small></strong>
                                <span class="is-megas">80<small>Megas</small></span>
                            </div>
                            <div class="is-complements is-clearfix">
                                <span style="width:100%;">
                                    <i class="fas fa-fax"></i>
                                    <small>1 Línea</small>
                                </span>
                            </div>
                            <div class="is-discount">
                                <strong>$50 Descuento</strong>
                                <span>de por vida a partir del mes 6</span>
                            </div>
                            <div class="is-total">
                                <span>Pronto Pago</span>
                                <strong>$599</strong>
                                <span>Precio de Lista</span>
                                <strong>$649</strong>
                                <small>Consulta términos y condiciones*</small>
                            </div>
                        </div>
                    </div>

                    <div class="column is-one-quarter is-doble-pack">
                        <div class="is-item is-pink">
                            <div class="is-title">
                                <strong>Diviértete</strong>
                                <span class="is-megas">40<small>Megas</small></span>
                            </div>
                            <div class="is-complements is-clearfix">
                                <span style="width:100%;">
                                    <i class="fas fa-fax"></i>
                                    <small>1 Línea</small>
                                </span>
                            </div>
                            <div class="is-discount">
                                <strong>$40 Descuento</strong>
                                <span>de por vida a partir del mes 6</span>
                            </div>
                            <div class="is-total">
                                <span>Pronto Pago</span>
                                <strong>$499</strong>
                                <span>Precio de Lista</span>
                                <strong>$549</strong>
                                <small>Consulta términos y condiciones*</small>
                            </div>
                        </div>
                    </div>-->

                </div>
            </div>

        </div>
    </div>
</section>

<?php require __DIR__ . '/components/footer.php'; ?>