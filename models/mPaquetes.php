<?php
class Paquetes
{
    public function getPaquetes($cve = null){
        $rt = [
            'tripleplay' => [],
            'dobleplay' => []
        ];

        //Get doblePlay
        $dataTP = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/models/dobleplay.json"), TRUE);
        foreach($dataTP as $dbpy) {
            if (($cve == null) || ($cve != null && $dbpy['cve'] == $cve)) {
                array_push($rt['dobleplay'], $dbpy);
            }
        }

        //Get TriplePlay
        $dataTP = json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/models/tripleplay.json"), TRUE);
        foreach($dataTP as $tpy) {
            if (($cve == null) || ($cve != null && $tpy['cve'] == $cve)) {
                array_push($rt['tripleplay'], $tpy);
            }
        }

        if ($cve == null) {
            return $rt;
        } else {
            if ($rt['dobleplay'] != []) {
                return $rt['dobleplay'][0];
            } else if ($rt['tripleplay'] != []) {
                return $rt['tripleplay'][0];
            }
        }
    }
}