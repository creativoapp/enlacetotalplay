$(document).ready(function () {
    var app = {
        'path': window.location.protocol + '//' + window.location.hostname + '/',
        'isvalid': true
    };

    //Img
    $('#promocionImg').on('change', function (e) {
        var uploadFile = this.files[0];
        if (!(/\.(jpg|png|gif)$/i).test(uploadFile.name)) {
            alert('El archivo a adjuntar no es una imagen');
        } else {
            var img = new Image();
            img.onload = function () {
                if (this.width.toFixed(0) != 780 && this.height.toFixed(0) != 520) {
                    alert('Las medidas deben ser: 780 * 520');
                    $('#promocionImg').removeClass('border-green').addClass('border-red');
                    app.isvalid = false;
                } else {
                    $('#promocionImg').removeClass('border-red').addClass('border-green');
                    app.isvalid = true;
                }
            };
            img.src = URL.createObjectURL(uploadFile);
            imgPreview(this, $('#is-preview'));
        }
    });

    //CVE
    $('#shortName').on('keyup', function (e) {
        var replace = $(this).val().toLowerCase().trim();
        replace = replace.replace('á', 'a');
        replace = replace.replace('é', 'e');
        replace = replace.replace('í', 'i');
        replace = replace.replace('ó', 'o');
        replace = replace.replace('ú', 'u');
        replace = replace.replace('ñ', 'n');
        replace = replace.replace(/[^a-zA-Z0-9]/g, '');
        $('#promocioncve').val(replace);
    });

    //FORM
    $('#frm-promocion').on('submit', function (e) {
        e.preventDefault();
        var val = true;
        if ($('#action').val() == 'create' && $('#promocionImg').val() == 0) {
            $('#promocionImg').removeClass('border-green').addClass('border-red');
            val = false;
        } else {
            $('#promocionImg').removeClass('border-red');
        }
        if ($('#promocionVigencia').val().trim() == '') {
            $('#promocionVigencia').removeClass('border-green').addClass('border-red');
            val = false;
        } else {
            $('#promocionVigencia').removeClass('border-red');
        }
        if ($('#promocioncve').val().trim() == '') {
            $('#promocioncve').removeClass('border-green').addClass('border-red');
            val = false;
        } else {
            $('#promocioncve').removeClass('border-red');
        }
        if ($('#promocionTitulo').val().trim() == '') {
            $('#promocionTitulo').removeClass('border-green').addClass('border-red');
            val = false;
        } else {
            $('#promocionTitulo').removeClass('border-red');
        }
        if ($('#shortName').val().trim() == '') {
            $('#shortName').removeClass('border-green').addClass('border-red');
            val = false;
        } else {
            $('#shortName').removeClass('border-red');
        }
        if (app.isvalid == false) {
            val = false;
        }
        if (val == true) {
            var data = new FormData(this);
            $.ajax({
                data: data,
                url: app.path + 'handlers/hdl-promocion.php',
                type: 'POST',
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    var res = eval('(' + data + ')');
                    if (res.status == 'validation') {
                        if (res.cve == false) {
                            $('#promocioncve').focus();
                            $('.msg-cve').text('Ya hay una promoción con este identifador');
                            $('#promocioncve').removeClass('border-green').addClass('border-red');
                        } else {
                            $('.msg-cve').text('');
                            $('#promocioncve').removeClass('border-red').addClass('border-green');
                        }
                        if (res.name == false) {
                            $('#shortName').focus();
                            $('.msg-name').text('Ya hay una promoción con este Nombre');
                            $('#shortName').removeClass('border-green').addClass('border-red');
                        } else {
                            $('.msg-name').text('');
                            $('#shortName').removeClass('border-red').addClass('border-green');
                        }
                    } else {
                        alert(res.msg);
                        $(location).attr('href', '/in/promociones');
                    }
                }
            });
        }
    });

    //DELETE
    $('.drop-promocion').on('click', function (e) {
        e.preventDefault();
        if (confirm('¿Estás Seguro(a) de querer eliminar esta Promoción?')) {
            var img = $(this).data('img');
            var id = $(this).data('id');
            var data = {
                'promocionImg': img,
                'idpromocion': id,
                'action': 'delete'
            };
            $.ajax({
                data: data,
                url: app.path + 'handlers/hdl-promocion.php',
                type: 'POST',
                success: function (data) {
                    var res = eval('(' + data + ')');
                    alert(res.msg);
                    location.reload();
                }
            });
        }
    });
});