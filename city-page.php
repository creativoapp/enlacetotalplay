<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view-home">
    <div class="is-comp-slider nivoSlider">
        <div id="is-comp-slider">
            <img src="<?= _IMG . 'slider/slide_01.jpg'; ?>">
            <img src="<?= _IMG . 'slider/slide_02.jpg'; ?>">
            <img src="<?= _IMG . 'slider/slide_03.jpg'; ?>">
            <img src="<?= _IMG . 'slider/slide_04.jpg'; ?>">
        </div>

        <div class="is-mask"></div>
        <div class="is-content">
            <div class="container">
                <div class="columns is-variable is-5">

                    <div class="column is-two-thirds">
                        <div class="is-banners">

                            <div id="is-comp-deals">
                                <?php foreach ($bannerList as $banner) { ?>
                                    <img src="<?= _IMG . 'slider/banner/' . $banner['img']; ?>" alt="<?= $banner['alt'] ?>" title="<?= $banner['title'] ?>">
                                <?php } ?>
                            </div>

                        </div>
                    </div>

                    <div class="column">
                        <?php require('components/cp-simple-contact.php'); ?>
                    </div>

                </div>
            </div>
        </div>

    </div>


    <!--ADDONS-->
    <div class="is-services">
        <div class="container">
            <div class="columns">
                <div class="column is-full p-4 has-background-white bdr-1">
                    <h1 class="has-text-centered is-title-home">Enlace Total<span class="cl-p">p</span><span class="cl-l">l</span><span class="cl-a">a</span><span class="cl-y">y</span></h1>
                    <h2 class="has-text-centered">Proveedores de Internet por fibra óptica,</br>servicios de telefonía y TV en <strong class="is-size-3"><?= $city_page['name']; ?></strong></h2>
                </div>
            </div>
            <div class="columns">

                <div class="column is-one-quarter is-paddingless">
                    <div class="is-item">
                        <i class="fas fa-wifi"></i>
                        <h3><a href="/totalplay-<?= $city_page['url']; ?>/internet">Internet</a></h3>
                        <p>Los datos viajan a a velocidad de la luz. Te ofrecemos el mejor <strong>Internet por fibra óptica</strong> de México. Disfruta de la más alta velocidad de conexión con hasta 500 Mbps, para que aproveches tus aplicaciones, tengas la mejor experiencia de navegación y accedas a servicios como Netflix, Prime Video y más; que sólo se pueden disfrutar con una conexión potente, estable y eficiente.</p>
                        <a href="/totalplay-<?= $city_page['url']; ?>/internet" class="is-simple-link">Internet Totalplay <i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>

                <div class="column is-one-quarter is-paddingless">
                    <div class="is-item">
                        <i class="fas fa-tv"></i>
                        <h3><a href="/totalplay-<?= $city_page['url']; ?>/television">Televisión</a></h3>
                        <p>Todos nuestros paquetes con servicios de TV brindan la mejor señal en HD, para que vivas la experiencia de la TV interactiva. Más de 260 canales con programación que incluyen las mejores películas, series, entretenimiento y noticias.</p>
                        <a href="/totalplay-<?= $city_page['url']; ?>/television" class="is-simple-link">TV Totalplay <i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>

                <div class="column is-one-quarter is-paddingless">
                    <div class="is-item">
                        <i class="fas fa-fax"></i>
                        <h3><a href="/totalplay-<?= $city_page['url']; ?>/telefonia">Telefonía</a></h3>
                        <p>Hablar sin preocupaciones ya es posible. Disfruta de los mejores paquetes de Internet y teléfono, para que también puedas hacer llamadas a números fijos y de larga distancia nacional e internacional sin límites y disfrutar de una buena cantidad de minutos a celular, para que te comuniques en cualquier momento.</p>
                        <a href="/totalplay-<?= $city_page['url']; ?>/telefonia" class="is-simple-link">Telefonía Totalplay <i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>

                <div class="column is-one-quarter is-paddingless">
                    <div class="is-item">
                        <i class="fas fa-box-open"></i>
                        <h3><a href="/totalplay-<?= $city_page['url']; ?>/paquetes">Paquetes</a></h3>
                        <p>Gracias a la tecnología de la fibra óptica, puedes disfrutar de múltiples servicios en una sola conexión confiable, veloz y de buena calidad. Conoce nuestros planes y paquetes Dobleplay y Tripleplay.</p>
                        <a href="/totalplay-<?= $city_page['url']; ?>/paquetes" class="is-simple-link">Paquetes Totalplay <i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <!--OVERVIEW-->
    <div class="container is-overview">
        <div class="columns">

            <div class="column is-half">
                <h4>Totalplay vino para quedarse</h4>
                <p>Totalplay es el proveedor de Internet por fibra óptica preferido en todo México. Destacados entre los mejores ISP del mercado, ofrecemos un servicio innovador. Cada vez más familias y empresas se suman a disfrutar de la mejor conectividad. Los paquetes de Totalplay más famosos son los paquetes Tripleplay, mismos que incluyen servicio de Internet, telefonía y TV.</p>
                <p>Calificados por nuestros clientes como el mejor <strong>proveedor de Internet empresarial</strong> y de uso personal, ofrecemos la mejor velocidad de Internet, los mejores programas de televisión digital, diversas aplicaciones para disfrutar y telefonía a bajo costo. Todo implementado a través de una potente conexión con fibra óptica.</p>
                <p>Nuestros paquetes Tripleplay son, sin duda, los mejores paquetes de Internet + TV + Telefonía en el mercado. Llegamos a revolucionar los servicios de telecomunicaciones haciendolos más eficientes. <span>¿Qué estás esperando? <a href="/totalplay-<?= $city_page['url']; ?>/contrata">¡Cámbiate ya!</a></span></p>
            </div>

            <div class="column is-half">
                <img src="/assets/img/totalplay-familiar.png" class="is-img-big is-img-centered">
            </div>

        </div>
    </div>

    <!--ADDITIONALS-->
    <div class="is-addons">
        <div class="container">
            <div class="columns is-variable is-5">

                <div class="column is-one-third">
                    <a href="/totalplay-<?= $city_page['url']; ?>/empresarial"><img src="/assets/img/totalplay-empresarial.jpg"></a>
                    <h3><a href="/totalplay-<?= $city_page['url']; ?>/empresarial">Para empresas</a></h3>
                    <p>Servicios de <strong>Internet empresarial</strong> y soluciones integrales para empresas, para que todo el equipo se mantenga conectado y productivo.</p>
                    <a href="/totalplay-<?= $city_page['url']; ?>/empresarial" class="is-simple-link">Internet empresarial <i class="fas fa-arrow-right"></i></a>
                </div>

                <div class="column is-one-third">
                    <a href="/totalplay-<?= $city_page['url']; ?>/promociones"><img src="/assets/img/totalplay-promociones.jpg"></a>
                    <h3><a href="/totalplay-<?= $city_page['url']; ?>/promociones">Promociones</a></h3>
                    <p>Conoce nuestros descuentos, ofertas y promociones disponibles. Consulta términos y condiciones.</p>
                    <a href="/totalplay-<?= $city_page['url']; ?>/promociones" class="is-simple-link">Promociones Totalplay <i class="fas fa-arrow-right"></i></a>
                </div>

                <div class="column is-one-third">
                    <a href="/totalplay-<?= $city_page['url']; ?>/canales"><img src="/assets/img/totalplay-canales.jpg"></a>
                    <h3><a href="/totalplay-<?= $city_page['url']; ?>/canales">Canales</a></h3>
                    <p>La mejor programación para que la diversión y el entretenimiento nunca se detengan. Conoce los canales incluídos en cada paquete de TV Totalplay.</p>
                    <a href="/totalplay-<?= $city_page['url']; ?>/canales" class="is-simple-link">Canales incluidos <i class="fas fa-arrow-right"></i></a>
                </div>

            </div>
        </div>
    </div>

</section>

<?php require __DIR__ . '/components/footer.php'; ?>