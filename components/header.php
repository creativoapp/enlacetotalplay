<?php
require('app/config.php');

//Cities 404
if ($pageLayout != 'default' && $city_page['name'] == null) {
    header('Location:/404');
}

$metas = $seoObj->metas($pageUri, $pageLayout, 'mx', $city_page);

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">

    <title><?= $metas['title']; ?></title>
    <meta name="keywords" content="" />
    <meta name="description" content="<?= $metas['description']; ?>" />

    <meta name="viewport" content="width=device-width" user-scalable="no" maximum-scale="5" />

    <link rel="shortcut icon" type="image/png" href="<?= _IMG . 'favicon.ico'; ?>" />

    <!-- STYLES -->
    <link defer rel="stylesheet" href="<?= _CSS . 'bulma.min.css'; ?>">
    <link defer rel="stylesheet" href="<?= _CSS . 'components.min.css'; ?>">
    <link rel="stylesheet" href="<?= _CSS . 'tp.min.css'; ?>">

    <script defer src="https://kit.fontawesome.com/94233a6903.js" crossorigin="anonymous"></script>

    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>

<body class="body">
    <header class="is-header">
        <div class="container">
            <div class="columns">

                <div class="column is-one-third is-brand">
                    <a href="/"><img src="<?= _IMG . 'enlace-totalplay.png'; ?>" class="is-img-big is-img-centered"></a>
                </div>

                <a href="#" id="btn-menu"><i class="fa fa-bars"></i></a>

                <div class="column is-two-thirds is-menu hidden">
                    <ul>
                        <li><a href="<?= $cityUriExtend == '' ? '/paquetes' : '/' . $cityUriExtend . '/paquetes'; ?>" class="is-orange">Paquetes</a></li>
                        <li><a href="/canales" class="is-green">Canales</a></li>
                        <li><a href="<?= $cityUriExtend == '' ? '/promociones' : '/' . $cityUriExtend . '/promociones'; ?>" class="is-purple">Promociones</a></li>
                        <li><a href="<?= $cityUriExtend == '' ? '/empresarial' : '/' . $cityUriExtend . '/empresarial'; ?>" class="is-red">Empresarial</a></li>
                        <li><a href="<?= $cityUriExtend == '' ? '/contrata' : '/' . $cityUriExtend . '/contrata'; ?>" class="is-blue">Contrata</a></li>
                        <li><a href="/contacto" class="is-pink">Contacto</a></li>
                    </ul>
                    <ul class="sub-menu-phone">
                        <li><a href="<?= $cityUriExtend == '' ? '/internet' : '/' . $cityUriExtend . '/internet'; ?>" class="is-link is-orange"><i class="fas fa-wifi"></i> <span>Internet</span></a></li>
                        <li><a href="<?= $cityUriExtend == '' ? '/television' : '/' . $cityUriExtend . '/television'; ?>" class="is-link is-green"><i class="fas fa-tv"></i> <span>Televisión</span></a></li>
                        <li><a href="<?= $cityUriExtend == '' ? '/telefonia' : '/' . $cityUriExtend . '/telefonia'; ?>" class="is-link is-blue"><i class="fas fa-fax"></i> <span>Telefonía</span></a></li>
                    </ul>
                </div>

            </div>
        </div>
    </header>
    <div class="is-sub-header">
        <div class="container">
            <div class="columns">

                <div class="column is-offset-one-third">
                    <a href="<?= $cityUriExtend == '' ? '/internet' : '/' . $cityUriExtend . '/internet'; ?>" class="is-link is-orange">
                        <i class="fas fa-wifi"></i>
                        <span>Internet</span>
                    </a>
                    <a href="<?= $cityUriExtend == '' ? '/television' : '/' . $cityUriExtend . '/television'; ?>" class="is-link is-green">
                        <i class="fas fa-tv"></i>
                        <span>Televisión</span>
                    </a>
                    <a href="<?= $cityUriExtend == '' ? '/telefonia' : '/' . $cityUriExtend . '/telefonia'; ?>" class="is-link is-blue">
                        <i class="fas fa-fax"></i>
                        <span>Telefonía</span>
                    </a>
                    <a href="https://www.facebook.com/TotalPlayCancunMx/" target="_blank" class="is-social is-facebook" title="Síguenos en Facebook"><i class="fab fa-facebook"></i></a>
                    <a href="https://twitter.com/ETotalplay" target="_blank" class="is-social is-twitter" title="Síguenos en Twitter"><i class="fab fa-twitter"></i></a>
                    <a href="tel:529982527702" class="is-phone"><i class="fas fa-phone"></i>(998) 252 7702</a>
                </div>

            </div>
        </div>
    </div>