<?php
$uriGet = filter_var($_GET['city'], FILTER_SANITIZE_STRING);
$uriArgs = explode('/', $uriGet);

$redirect = 'site.enlacetotalplay.com';
if(count($uriArgs) == 2) {
    $redirect = 'site.enlacetotalplay.com/'.$uriArgs[0];
}

Header( "HTTP/1.1 301 Moved Permanently" ); 
Header( "Location: ".$redirect );
exit(); 