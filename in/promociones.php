<?php
include('components/header.php');
$mPromociones = new Promociones();
$cityUriExtend = null;
if (isset($_GET['fil-city'])) {
    $cityUriExtend = $_GET['fil-city'];
}
$promoList = $mPromociones->getPromociones($cityUriExtend, false);
?>
<div class="container">
    <div class="columns">
        <div class="column is-full">
            <form action="promociones" class="form-filter" method="GET">
                <h1 class="is-size-4">Promociones</h1>
                <fieldset class="is-vr">
                    <label for="">Ciudad</label>
                    <select name="fil-city" id="fil-city">
                        <option value="">General</option>
                        <?php
                        foreach ($cities as $key => $city) {
                            $selected = '';
                            if ($key == $cityUriExtend) {
                                $selected = 'selected';
                            }
                            echo '<option value="' . $city->url . '" ' . $selected . ' >' . $city->text . '</option>' . "\r\n";
                        }
                        ?>
                    </select>
                </fieldset>
                <fieldset>
                    <label for="">&nbsp;</label>
                    <input type="submit" value="Buscar">
                </fieldset>
                <fieldset class="is-r">
                    <label for="">&nbsp;</label>
                    <a href="/in/form/promocion" class="button is-info is-outlined">Agregar Promoción</a>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="columns is-multiline">
        <?php foreach ($promoList as $promocion) { ?>
            <?php if ($promocion['promocionCity'] == $cityUriExtend) { ?>
                <?php $gray = ($promocion['promocionVigencia'] <= date('Y-m-d') ? 'has-background-white-ter' : '') ?>
                <div class="column is-half">
                    <div class="is-item <?= $gray ?>">
                        <div class="is-img .is-center">
                            <img src="<?= _IMG . 'promociones/' . $promocion['promocionImg'] ?>">
                        </div>
                        <div class="is-info w-img">
                            <p><strong>Nombre: </strong><?= $promocion['shortName'] ?></p>
                            <p><strong>Título: </strong><small><?= $promocion['promocionTitulo'] ?></small></p>
                            <p><strong>Vigencia: </strong><small><?= $promocion['promocionVigencia'] ?></small></p>
                            <p><strong>Ciudad: </strong><small><?= ($promocion['promocionCity'] == '' ? 'General' : $promocion['promocionCity']) ?></small></p>
                        </div>
                        <div class="is-settings">
                            <a href="/in/form/promocion?action=edit&cve=<?= $promocion['promocioncve'] ?>" title="Modificar"><i class="fa fa-pencil"></i></a>
                            <a href="#" class="drop-promocion" data-img="<?= $promocion['promocionImg'] ?>" data-id="<?= $promocion['idpromocion'] ?>" title="Elimiar"><i class="fas fa-trash"></i></a>
                        </div>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
</div>
<?php include('components/footer.php'); ?>