<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view is-view-service">

    <!--OVERVIEW-->
    <div class="hero-body">
    <div class="container">
        <div class="columns">
            
            <div class="column is-half is-overview">
                <h1 class="is-title-home">Internet con Total<span class="cl-p">p</span><span class="cl-l">l</span><span class="cl-a">a</span><span class="cl-y">y</span></h1>
                <h2 class="is-size-5">La velocidad de conexión que siempre soñaste.</h2>
                <p class="is-pr-big has-text-justified">Gracias a la potente conexión lograda a través de la fibra óptica, tus datos viajan a la velocidad de la luz, ¡literalmente! Un rápido haz de luz lleva tus solicitudes y te regresa los datos en tiempo record. Además, esta conexión es, por naturaleza, menos propensa a interferencias por tormentas eléctricas o fenómenos electromagnéticos. Es decir, navegarás rápido y sin problemas.</p>
                <p class="is-pr-big has-text-justified">Nuestros paquetes de Internet para empresas y hogares a excelentes precios, con grandes beneficios y la mejor tecnología te brindarán el servicio que mereces.</p>
            
                <div class="is-clearfix">
                    <div class="is-item is-item-internet">
                        <i class="fas fa-check-circle"></i>
                        <h3 class="is-size-6 dpy-inl">Dispositivos múltiples</h3>
                        <p class="has-text-justified">Ancho de banda que te permitirá navegar en varios dispositivos al mismo tiempo con <a href="/paquetes">conexiones desde 20 Mbps.</a></p>
                    </div>
                    <div class="is-item is-item-internet">
                        <i class="fas fa-check-circle"></i>
                        <h3 class="is-size-6 dpy-inl">Navega sin Interrupciones</h3>
                        <p class="has-text-justified">Todo tu hogar en línea: Termina la videollamada mientras tu familia juega y descarga a gran velocidad con <a href="/paquetes">conexiones desde 100 Mbps</a>.</p>
                    </div>
                    <div class="is-item is-item-internet">
                        <i class="fas fa-check-circle"></i>
                        <h3 class="is-size-6 dpy-inl">Internet realmente veloz</h3>
                        <p class="has-text-justified">Velocidades de hasta 500 Mbps.</p>
                    </div>
                </div>

            </div>

            <div class="column is-half is-overview dv-center-content">
                <img src="/assets/img/internet-totalplay.png" class="is-img-centered">
            </div>

        </div>
    </div>
    </div>

    <div class="is-row-service">
        <div class="container">
            <div class="columns">
            
                <div class="column is-one-third is-item is-item-internet">
                    <h4><i class="fas fa-arrow-circle-down"></i>Descargas</h4>
                    <p>¡Descargas al instante! A partir de ahora puedes disfrutar de la verdadera banda ancha que sólo Totalplay ofrece; aprovecha al máximo el poder que te dan los 500 Mbps de velocidad para bajar música, fotos y mucho más.</p>
                </div>

                <div class="column is-one-third is-item is-item-internet">
                    <h4><i class="fas fa-arrow-circle-up"></i>Subida</h4>
                    <p>Se el primero en actualizar tus redes sociales. Comparte fotos, carga videos y adjunta archivos velozmente. Conexión efectiva para mantenerte en contacto. Hola, mundo.</p>
                </div>

                <div class="column is-one-third is-item is-item-internet">
                    <h4><i class="fas fa-gamepad"></i>Gamers</h4>
                    <p>Juegos en línea a toda velocidad. Rompe récords y conviértete en el líder mientras te diviertes en línea. Al fín, una conexión sin lag. ¡Hora de demostrar tus habilidades! <small>(algunos sitios pueden requerir la contratación de una IP fija de manera independiente)</small></p>
                </div>

            </div>
        </div>
    </div>

</section>

<?php require __DIR__ . '/components/footer.php'; ?>