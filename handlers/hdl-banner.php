<?php
if (isset($_POST['action'])) {
    include($_SERVER['DOCUMENT_ROOT'] . '/models/mBanner.php');
    $mBanner = new Banner();
    if ($_POST['action'] == 'create' || $_POST['action'] == 'edit') {
        $data = [
            'bannerSrc' => $_POST['bannerSrc-name'],
            'bannerAlt' => $_POST['bannerAlt'],
            'bannerTitle' => $_POST['bannerTitle'],
            'bannerCity' => $_POST['bannerCity']
        ];
    }
    if ($_POST['action'] == 'create') {
        $data['bannerSrc'] = uploadImage($_POST['action']);
        $data['bannerOrden'] = $mBanner->lastOrder($data['bannerCity']);
        echo $mBanner->createBanner($data);
    }
    if ($_POST['action'] == 'edit') {
        $data['bannerSrc'] = uploadImage($_POST['action']);
        if ($data['bannerCity'] != $_POST['last-city']) {
            $data['bannerOrden'] = $mBanner->lastOrder($data['bannerCity']);
        }
        echo $mBanner->editBanner($_POST['idbanner'], $data);
    }
    if ($_POST['action'] == 'delete') {
        $file = $_SERVER['DOCUMENT_ROOT'] . '/assets/img/slider/banner/'.$_POST['bannerSrc'];
        if (file_exists($file)) {
            unlink($file);
        }
        echo $mBanner->deleteBanner($_POST['idbanner']);
    }

    if ($_POST['action'] == 'order') {
        if ($mBanner->validOrder($_POST['bannerOrden'], $_POST['bannerCity']) == false){
            echo json_encode(['status' => 'failed', 'msg' => 'Ya existe otro elemento con este numero.']);
        } else {
            echo $mBanner->editOrden($_POST['idbanner'], $_POST['bannerOrden']);
        }
    }
}

function uploadImage($action)
{
    $dir_subida = $_SERVER['DOCUMENT_ROOT'] . '/assets/img/slider/banner/';
    if ($action == 'create') {
        return upImg($dir_subida);
    }
    if ($action == 'edit') {
        if (trim($_FILES['bannerSrc']['name']) != '') {
            if (file_exists($dir_subida . $_POST['bannerSrc-name'])) {
                unlink($dir_subida . $_POST['bannerSrc-name']);
            }
            return upImg($dir_subida);
        } else {
            return $_POST['bannerSrc-name'];
        }
    }
}

function upImg($dir_subida)
{
    $_FILES['bannerSrc']['name'] = clearImgName($_FILES['bannerSrc']['name']);
    $fichero_subido = $dir_subida . basename($_FILES['bannerSrc']['name']);
    if (file_exists($fichero_subido)) {
        $exp = explode('.', $_FILES['bannerSrc']['name']);
        $limexp = count($exp) - 1;
        $_FILES['bannerSrc']['name'] = $exp[0].'_'.time().'.'.$exp[$limexp];
        $fichero_subido = $dir_subida . basename($_FILES['bannerSrc']['name']);
    }
    if (move_uploaded_file($_FILES['bannerSrc']['tmp_name'], $fichero_subido)) {
        return trim(basename($_FILES['bannerSrc']['name']));
    } else {
        return '';
    }
}

function clearImgName($name)
{
    $name = str_replace(' ', '_', $name);
    $name = str_replace('á', 'a', $name);
    $name = str_replace('é', 'e', $name);
    $name = str_replace('í', 'i', $name);
    $name = str_replace('ó', 'o', $name);
    $name = str_replace('ú', 'u', $name);
    $name = str_replace('Á', 'A', $name);
    $name = str_replace('É', 'E', $name);
    $name = str_replace('Í', 'I', $name);
    $name = str_replace('Ó', 'O', $name);
    $name = str_replace('Ú', 'U', $name);
    $name = str_replace('Ñ', 'N', $name);
    $name = str_replace('ñ', 'n', $name);
    $name = preg_replace('([^A-Za-z0-9 ._-])', '', $name);
    return $name;
}
