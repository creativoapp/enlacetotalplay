<?php include($_SERVER['DOCUMENT_ROOT'].'/in/app/config.php') ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="<?= _CSS . 'bulma.min.css'; ?>">
    <link rel="stylesheet" href="<?= _CSS . 'components.min.css'; ?>">
    <link rel="stylesheet" href="<?= _CSS . 'dashboard.min.css'; ?>">

    <link rel="shortcut icon" type="image/png" href="<?= _IMG . 'favicon.ico'; ?>" />

    <script src="https://kit.fontawesome.com/94233a6903.js" crossorigin="anonymous"></script>

    <title>Document</title>
</head>

<body>
    <header>
        <nav class="navbar is-link" role="navigation" aria-label="main navigation">
            <div class="navbar-brand">
                <a class="navbar-item" href="/" target="__blank">
                    <img src="<?= _IMG.'enlace-totalplay.png' ?>" width="112" height="28">
                </a>
                <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                </a>
            </div>

            <div id="navbarBasicExample" class="navbar-menu">
                <div class="navbar-start">
                    <a class="navbar-item" href="/in/banners">
                        Banners
                    </a>
                    <a class="navbar-item" href="/in/promociones">
                        Promociones
                    </a>
                    <div class="navbar-item has-dropdown is-hoverable">
                        <a class="navbar-link">
                            Paquetes
                        </a>
                        <div class="navbar-dropdown">
                            <a class="navbar-item" href="/in/paquetes?type=dp">
                                Dobleplay
                            </a>
                            <hr class="navbar-divider">
                            <a class="navbar-item" href="/in/paquetes?type=tp">
                                Tripleplay
                            </a>
                        </div>
                    </div>
                </div>

                <div class="navbar-end">
                    <div class="navbar-item">
                        <div class="buttons">
                            <a class="button is-danger" href="/in/">
                                Salir
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </header>