<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view-empresa">
    <div class="is-comp-slider is-comp-slider-empresa nivoSlider">

        <div id="is-comp-slider">
            <img src="<?=_IMG.'slider/empresarial_01.jpg';?>">
            <img src="<?=_IMG.'slider/empresarial_02.jpg';?>">
        </div>

        <div class="is-mask"></div>
        <div class="is-content">
            <div class="container">
                <div class="columns is-variable is-5">
                    
                    <div class="column is-4 is-offset-8">
                        <?php require('components/cp-simple-contact.php'); ?>
                    </div>

                </div>
            </div>
        </div>

    </div>

        
    <!--OVERVIEW-->
    <div class="container is-overview">
        <div class="columns">
        
            <div class="column is-half">
                <h1 class="is-title-home">Total<span class="cl-p">p</span><span class="cl-l">l</span><span class="cl-a">a</span><span class="cl-y">y</span> Empresarial</h1>
                <p class="has-text-justified">Brindamos los mejores servicios integrales en telecomunicaciones para que tu empresa se mantenga en su funcionamiento más óptimo. Obtén un servicio de Internet para empresas brindado a través de tecnología con fibra óptica. Los datos viajan mediante la luz, haciéndolo el medio más eficiente de conexión. Evítate las interrupciones de trabajo por baja o nula velocidad de Internet.</p>
            </div>

            <div class="column is-half">
                <img src="/assets/img/totalplay-empresas.png" class="is-img-big is-img-centered">
            </div>

        </div>
    </div>

    <!--ADDITIONALS-->
    <div class="is-addons">
        <div class="container">
            <div class="columns is-variable is-5">
            
                <div class="column is-one-third">
                    <img src="/assets/img/educacion-totalplay.jpg">
                    <h2>Sector Educativo</h2>
                    <p>Redes eficientes para los centros de estudios y academias con las más altas exigencias en tecnología.</p>
                </div>

                <div class="column is-one-third">
                    <img src="/assets/img/gobierno-totalplay.jpg">
                    <h2>Sector Gubernamental</h2>
                    <p>Ayudamos a mejorar la disponibilidad de los servicios públicos brindando soluciones acordes a cada organismo.</p>
                </div>

                <div class="column is-one-third">
                    <img src="/assets/img/comercial-totalplay.jpg">
                    <h2>Sector Comercial</h2>
                    <p>Servicios para negocios locales. Manténgase conectado con el personal de su establecimiento.</p>
                </div>

            </div>
        </div>
    </div>


</section>

<?php require __DIR__ . '/components/footer.php'; ?>