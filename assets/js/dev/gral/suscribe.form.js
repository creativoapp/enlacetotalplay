$(document).ready(function () {
    $('#cp-newsletter-button').on('click', function (e) {
        e.preventDefault();
        var email = $('#cp-newsletter').val().trim();
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var from = new String(window.location);
        if (email != '' && regex.test(email)) {
            $.ajax({
                data: {'email': email, 'from': from},
                url: window.location.protocol + '//' + window.location.hostname + '/handlers/hdl-suscripcion.php',
                type: 'POST',
                beforeSend: function () {
                    $('#cp-newsletter-button').val('GUARDANDO...');
                    $('#cp-newsletter').attr("disabled", true);
                    $('#cp-newsletter').css('background', '#fff');
                    $('#cp-newsletter-button').attr("disabled", true);
                },
                success: function (rs) {
                    console.log(rs);
                    if (rs == true) {
                        $('#cp-newsletter-button').val('SUSCRITO');
                        $('#cp-newsletter-button').css('background', '#9444b3');
                    }
                    if (rs == false) {
                        $('#cp-newsletter').focus();
                        $('#cp-newsletter-button').val('SUSCRIBIRME');
                        $('#cp-newsletter').attr("disabled", false);
                        $('#cp-newsletter-button').attr("disabled", false);
                    }
                }
            });
        } else {
            $('#cp-newsletter').focus();
        }
    });
});