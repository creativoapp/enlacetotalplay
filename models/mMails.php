<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require $_SERVER['DOCUMENT_ROOT'] . '/PHPMailer/src/Exception.php';
require $_SERVER['DOCUMENT_ROOT'] . '/PHPMailer/src/PHPMailer.php';
require $_SERVER['DOCUMENT_ROOT'] . '/PHPMailer/src/SMTP.php';

class mMails
{
    public function sendMail($to, $subject, $message)
    {
        try {
            $mail = new PHPMailer();
            //SMTP Configuration
            $mail->isSMTP();
            $mail->Host = 'smtp.mailtrap.io';
            $mail->SMTPAuth = true;
            $mail->Username = '5ed680e4ce9cea';
            $mail->Password = 'affb01e865d165';
            $mail->SMTPSecure = 'tls';
            $mail->Port = 2525;
            //Headers
            $mail->CharSet = 'UTF-8';
            $mail->setFrom('info@enlacetotalplay.com', 'Enlace TotalPlay');
            $mail->addAddress($to);
            //Subjet
            $mail->isHTML(true);
            $mail->Subject = $subject;
            //Message
            $mail->Body = $message;
            //Validate Mail
            if ($mail->send()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    public function ClientTemplateHeader()
    {
        return '
        <div style="overflow: hidden;">
            <div style="width: 700px; background: #39455b;">
                <a title="Sitio Web | Enlace Total Play" href="http://www.enlacetotalplay.com/" style="display: block; width: 50%; box-sizing: border-box; padding: 15px;"><img style="width: 100%;" src="http://www.enlacetotalplay.com/assets/img/enlace-totalplay.png" alt=""></a>
            </div>
            <div style="width: 700px; background: #fff; box-sizing: border-box; padding: 15px; overflow: hidden; font-family: '. "'Noto Sans', 'Helvetica'". ', Arial, sans-serif;">';
    }

    public function ClientTemplateFooter()
    {
        return '
            </div>
                <div style="width: 700px; background: #39455b; box-sizing: border-box; padding: 15px; overflow: hidden;">
                    <p style="display: block; width: 223.33px; float: left; text-align: center;"><a style="font-size: 16px; font-weight: 700; color: #ffc02c; font-family: ' . "'Noto Sans', 'Helvetica'" . ', Arial, sans-serif; text-decoration: none;" target="__blank" title="Internet" href="http://www.enlacetotalplay.com/internet">Internet</a></p>
                    <p style="display: block; width: 223.33px; float: left; text-align: center;"><a style="font-size: 16px; font-weight: 700; color: #d8e526; font-family: ' . "'Noto Sans', 'Helvetica'" . ', Arial, sans-serif; text-decoration: none;" target="__blank" title="Televisión" href="http://www.enlacetotalplay.com/television">Televisión </a></p>
                    <p style="display: block; width: 223.33px; float: left; text-align: center;"><a style="font-size: 16px; font-weight: 700; color: #1da1f2; font-family: ' . "'Noto Sans', 'Helvetica'" . ', Arial, sans-serif; text-decoration: none;" target="__blank" title="Telefonía" href="http://www.enlacetotalplay.com/telefonia">Telefonía </a></p>
                    <div style="clear: both;"></div>
                    <p style="margin: 15px 0px 3px 0px; text-align: center;"><a title="Llamanos" href="tel:529982527702" style="font-size: 10px; color: #ff4d9c; font-family: ' . "'Noto Sans', 'Helvetica'" . ', Arial, sans-serif; text-decoration: none; font-weight: 700;">Tel: (998) 252 7702</a></p>
                    <p style="margin: 3px 0px; text-align: center;"><a title="Envianos un Correo Electrónico" href="mailto:info@enlacetotalplay.com" style="font-size: 10px; color: #9444b3; font-family: ' . "'Noto Sans', 'Helvetica'" . ', Arial, sans-serif; text-decoration: none; font-weight: 700;">info@enlacetotalplay.com</a></p>
                </div>
            </div>';
    }

    public function templateHeader($title = null)
    {
        $header = '
        <div style="overflow: hidden;">
            <div style="width: 700px; background: #39455b; overflow: hidden;">
                <a title="Sitio Web | Enlace Total Play" href="http://www.enlacetotalplay.com/" style="display: block; width: 50%; box-sizing: border-box; padding: 15px; float: left;"><img style="width: 100%;" src="http://www.enlacetotalplay.com/assets/img/enlace-totalplay.png" alt=""></a>
                <h1 style="width: 50%; float: right; color: #fff; text-align: center; font-size: 25px; font-family: ' . "'Noto Sans', 'Helvetica'" . ', Arial, sans-serif;">{TITLE}</h1>
            </div>
            <div style="width: 700px; background: #fff; box-sizing: border-box; padding: 15px; overflow: hidden; font-family: '. "'Noto Sans', 'Helvetica'". ', Arial, sans-serif;">';

        if (trim($title) != '') {
            $header = str_replace('{TITLE}', $title, $header);
        } else{
            $header = str_replace('{TITLE}', '', $header);
        }

        return $header;
    }

    public function templateFooter()
    {
        return '
            </div>
            <div style="width: 700px; background: #39455b; box-sizing: border-box; padding: 15px; overflow: hidden;"></div>
        </div>';
    }

    public function getIpClient()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            return $_SERVER['HTTP_CLIENT_IP'];
        } else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else if (!empty($_SERVER['HTTP_X_FORWARDED'])) {
            return $_SERVER['HTTP_X_FORWARDED'];
        } else if (!empty($_SERVER['HTTP_FORWARDED_FOR'])) {
            return $_SERVER['HTTP_FORWARDED_FOR'];
        } else if (!empty($_SERVER['HTTP_FORWARDED'])) {
            return $_SERVER['HTTP_FORWARDED'];
        } else {
            return $_SERVER['REMOTE_ADDR'];
        }
    }
}
