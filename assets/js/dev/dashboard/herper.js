//Preview Imagen all site
function imgPreview(inputFile, imgPreview) {
    if (inputFile.files && inputFile.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            imgPreview.attr('src', e.target.result);
        }
        reader.readAsDataURL(inputFile.files[0]);
    } else {
        imgPreview.attr('src', "");
    }
}

$('.text-cve').on('input', function() {
    var replace = $(this).val().toLowerCase();
    replace = replace.replace('á', 'a');
    replace = replace.replace('é', 'e');
    replace = replace.replace('í', 'i');
    replace = replace.replace('ó', 'o');
    replace = replace.replace('ú', 'u');
    replace = replace.replace('ñ', 'n');
    replace = replace.replace(/[^a-zA-Z0-9]/g, '');
    $(this).val(replace);
});