<?php
include('components/header.php');
$mBanner = new Banner();
$cityUriExtend = null;
if (isset($_GET['fil-city'])) {
    $cityUriExtend = $_GET['fil-city'];
}
$bannerList = $mBanner->getBanner($cityUriExtend);
?>
<div class="container">
    <div class="columns">
        <div class="column is-full">
            <form action="banners" class="form-filter" method="GET">
                <h1 class="is-size-4">Banners</h1>
                <fieldset class="is-vr">
                    <label for="">Ciudad</label>
                    <select name="fil-city" id="fil-city">
                        <option value="">General</option>
                        <?php
                        foreach ($cities as $key => $city) {
                            $selected = '';
                            if ($key == $cityUriExtend) {
                                $selected = 'selected';
                            }
                            echo '<option value="' . $city->url . '" ' . $selected . ' >' . $city->text . '</option>' . "\r\n";
                        }
                        ?>
                    </select>
                </fieldset>
                <fieldset>
                    <label for="">&nbsp;</label>
                    <input type="submit" value="Buscar">
                </fieldset>
                <fieldset class="is-r">
                    <label for="">&nbsp;</label>
                    <a href="/in/form/banner" class="button is-info is-outlined">Agregar Banner</a>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="columns is-multiline">
        <?php foreach ($bannerList as $banner) { ?>
            <?php if ($banner['city'] == $cityUriExtend) { ?>
                <div class="column is-half">
                    <div class="is-item">
                        <div class="is-img .is-center">
                            <img src="<?= _IMG . 'slider/banner/' . $banner['img'] ?>">
                        </div>
                        <div class="is-info w-img">
                            <p><strong>Title: </strong><?= $banner['title'] ?></p>
                            <p><strong>Alt: </strong><?= $banner['alt'] ?></p>
                            <p><strong>Orden: </strong><input type="number" min="1" class="min-input ordn-banner" data-order="<?= $banner['order'] ?>" data-id="<?= $banner['id'] ?>" data-city="<?= $banner['city'] ?>" value="<?= $banner['order'] ?>"></p>
                            <p><strong>Ciudad: </strong><?= ($banner['city'] == '' ? 'General' : $banner['city']) ?></p>
                        </div>
                        <div class="is-settings">
                            <a href="/in/form/banner?action=edit&id=<?= $banner['id'] ?>" title="Modificar"><i class="fa fa-pencil"></i></a>
                            <a href="#" class="drop-banner" data-img="<?= $banner['img'] ?>" data-id="<?= $banner['id'] ?>" title="Elimiar"><i class="fas fa-trash"></i></a>
                        </div>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
</div>
<?php include('components/footer.php'); ?>