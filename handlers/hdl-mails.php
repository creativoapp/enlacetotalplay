<?php
date_default_timezone_set('America/Cancun');
require($_SERVER['DOCUMENT_ROOT'] . '/models/mMails.php');
$mmails = new mMails();
$datetime = date('Y-m-d H:i:s');

//CONTACTO
if (isset($_POST['type']) && $_POST['type'] == 'contact') {
    //SEND CLIENT MAIL
    $bodyMail = $mmails->ClientTemplateHeader() . '<p style="font-family: ' . "'Noto Sans', 'Helvetica'" . ', Arial, sans-serif; color: #9444b3; font-size: 20px; text-align: center;">Gracias por contactarte con <strong>Enlace TotalPlay</strong>.<br>Nos pondremos en contacto con tigo.</p>' . $mmails->ClientTemplateFooter();

    $mmails->sendMail($_POST['email'], 'Enlace TotalPlay - Contacto', $bodyMail);

    //SEND INFO
    $title = 'Nuevo Contacto';
    $from = $_POST['from'];
    $include = '';
    if (trim($_POST['promo']) != '') {
        require($_SERVER['DOCUMENT_ROOT'] . '/models/mPromociones.php');
        $mPromociones = new Promociones();
        $promocion = $mPromociones->getPromocion($_POST['promo']);
        $title = 'Nuevo Contacto para Promoción';
        $include = '<p style="font-family: ' . "'Noto Sans', 'Helvetica'" . ', Arial, sans-serif; color: #9444b3; text-align: center;"><strong>Promoción:</strong> '.$promocion['shortName'].'</p>';
        $from = $_POST['from-promo'];
    }
    $bodyMail = $mmails->templateHeader($title) . '<p style="font-family: ' . "'Noto Sans', 'Helvetica'" . ', Arial, sans-serif; color: #9444b3; text-align: center;"><strong>Nombre:</strong> ' . $_POST['name'] . '</p>
    <p style="font-family: ' . "'Noto Sans', 'Helvetica'" . ', Arial, sans-serif; color: #9444b3; text-align: center;"><strong>Correo Electrónico:</strong> ' . $_POST['email'] . '</p>
    <p style="font-family: ' . "'Noto Sans', 'Helvetica'" . ', Arial, sans-serif; color: #9444b3; text-align: center;"><strong>Número Telefónico:</strong> ' . $_POST['number'] . '</p>
    ' . $include . '
    <p style="font-family: ' . "'Noto Sans', 'Helvetica'" . ', Arial, sans-serif; color: #9444b3; text-align: center;"><strong>Mensaje:</strong></br>' . $_POST['messaje'] . '</p>
    <p style="font-family: ' . "'Noto Sans', 'Helvetica'" . ', Arial, sans-serif; color: #9444b3; text-align: center;"><strong>URL de Envio:</strong> ' . $from . '</p>
    <p style="font-family: ' . "'Noto Sans', 'Helvetica'" . ', Arial, sans-serif; color: #9444b3; text-align: center;"><small>' . $datetime . '</small></p>' . $mmails->templateFooter();

    echo $mmails->sendMail('info@enlacetotalplay.com', 'Enlace TotalPlay - ' . $title, $bodyMail);
}

//CONTACTO SIMPLE
if (isset($_POST['type']) && $_POST['type'] == 'simple-contact') {
    //SEND CLIENT MAIL
    $bodyMail = $mmails->ClientTemplateHeader() . '<p style="font-family: ' . "'Noto Sans', 'Helvetica'" . ', Arial, sans-serif; color: #9444b3; font-size: 20px; text-align: center;">Gracias por contactarte con <strong>Enlace TotalPlay</strong>.<br>Nos pondremos en contacto con tigo.</p>' . $mmails->ClientTemplateFooter();

    $mmails->sendMail($_POST['email'], 'Enlace TotalPlay - Contacto', $bodyMail);

    //SEND INFO
    $bodyMail = $mmails->templateHeader('Nuevo Contacto') . '<p style="font-family: ' . "'Noto Sans', 'Helvetica'" . ', Arial, sans-serif; color: #9444b3; text-align: center;"><strong>Nombre:</strong> ' . $_POST['name'] . '</p>
    <p style="font-family: ' . "'Noto Sans', 'Helvetica'" . ', Arial, sans-serif; color: #9444b3; text-align: center;"><strong>Correo Electrónico:</strong> ' . $_POST['email'] . '</p>
    <p style="font-family: ' . "'Noto Sans', 'Helvetica'" . ', Arial, sans-serif; color: #9444b3; text-align: center;"><strong>Número Telefónico:</strong> ' . $_POST['number'] . '</p>
    <p style="font-family: ' . "'Noto Sans', 'Helvetica'" . ', Arial, sans-serif; color: #9444b3; text-align: center;"><strong>Código Postal:</strong> ' . $_POST['codigo'] . '</p>
    <p style="font-family: ' . "'Noto Sans', 'Helvetica'" . ', Arial, sans-serif; color: #9444b3; text-align: center;"><strong>URL de Envio:</strong> ' . $_POST['from'] . '</p>
    <p style="font-family: ' . "'Noto Sans', 'Helvetica'" . ', Arial, sans-serif; color: #9444b3; text-align: center;"><small>' . $datetime . '</small></p>' . $mmails->templateFooter();

    echo $mmails->sendMail('info@enlacetotalplay.com', 'Enlace TotalPlay - Nuevo Contacto', $bodyMail);
}

//CONTRATACION
if (isset($_POST['type']) && $_POST['type'] == 'contrata-form') {
    //SEND CLIENT MAIL
    $bodyMail = $mmails->ClientTemplateHeader() . '<p style="font-family: ' . "'Noto Sans', 'Helvetica'" . ', Arial, sans-serif; color: #9444b3; font-size: 20px; text-align: center;">Gracias por contactarte con <strong>Enlace TotalPlay</strong>.<br>Nos pondremos en contacto con tigo.</p>' . $mmails->ClientTemplateFooter();

    $mmails->sendMail($_POST['email'], 'Enlace TotalPlay - Contratación', $bodyMail);

    //Info Mail
    $bodyMail = $mmails->templateHeader('Contratación TotalPlay') . '
    <div style="display: block;width: 100%;float: left;box-sizing: border-box;padding: 5px;text-align: center; overflow: hidden;">
        <div style="width: 100%;background: #39455b;color: #fff;box-sizing: border-box;padding: 5px;">Nombre</div>
        <p>' . $_POST['name'] . '&nbsp;</p>
    </div>
    <div style="display: block;width: 50%;float: left;box-sizing: border-box;padding: 5px;text-align: center; overflow: hidden;">
        <div style="width: 100%;background: #39455b;color: #fff;box-sizing: border-box;padding: 5px;">Correo Electrónico</div>
        <p>' . $_POST['email'] . '&nbsp;</p>
    </div>
    <div style="display: block;width: 50%;float: left;box-sizing: border-box;padding: 5px;text-align: center; overflow: hidden;">
        <div style="width: 100%;background: #39455b;color: #fff;box-sizing: border-box;padding: 5px;">Número Telefónico</div>
        <p>' . $_POST['number'] . '&nbsp;</p>
    </div>
    <div style="display: block;width: 50%;float: left;box-sizing: border-box;padding: 5px;text-align: center; overflow: hidden;">
        <div style="width: 100%;background: #39455b;color: #fff;box-sizing: border-box;padding: 5px;">Ciudad</div>
        <p>' . $_POST['city'] . '&nbsp;</p>
    </div>
    <div style="display: block;width: 50%;float: left;box-sizing: border-box;padding: 5px;text-align: center; overflow: hidden;">
        <div style="width: 100%;background: #39455b;color: #fff;box-sizing: border-box;padding: 5px;">Código Postal</div>
        <p>' . $_POST['codigo'] . '&nbsp;</p>
    </div>
    <div style="display: block;width: 50%;float: left;box-sizing: border-box;padding: 5px;text-align: center; overflow: hidden;">
        <div style="width: 100%;background: #39455b;color: #fff;box-sizing: border-box;padding: 5px;">Interes</div>
        <p>' . $_POST['interesting'] . '&nbsp;</p>
    </div>
    <div style="display: block;width: 50%;float: left;box-sizing: border-box;padding: 5px;text-align: center; overflow: hidden;">
        <div style="width: 100%;background: #39455b;color: #fff;box-sizing: border-box;padding: 5px;">¿Cómo se entero de nosotros?</div>
        <p>' . (($_POST['medio'] == 'Otro') ? $_POST['otro'] : $_POST['medio']) . '&nbsp;</p>
    </div>
    <div style="display: block;width: 100%;float: left;box-sizing: border-box;padding: 5px;text-align: center; overflow: hidden;">
        <div style="width: 100%;background: #39455b;color: #fff;box-sizing: border-box;padding: 5px;">Mensaje</div>
        <p>' . $_POST['message'] . '&nbsp;</p>
    </div>
    </br>
    <p style="color: #9444b3; text-align: center;"><strong>URL de Envio:</strong> ' . $_POST['from'] . '</p>
    <p style="color: #9444b3; text-align: center;"><small>' . $datetime . '</small></p>
    ' . $mmails->templateFooter();

    echo $mmails->sendMail('info@enlacetotalplay.com', 'Enlace TotalPlay - Nueva Contratación', $bodyMail);
}