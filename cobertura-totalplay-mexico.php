<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view is-view-cobertura">

    <div class="container">
        <div class="columns is-multiline">
            
            <div class="column is-full">
                <h1>Cobertura TotalPlay en México</h1>
                
                <iframe src="https://www.totalplay.com.mx/web/mapas/index" title="Cobertura TotalPlay en México"></iframe> 
                
            </div>


        </div>
    </div>

</section>

<?php require __DIR__ . '/components/footer.php'; ?>