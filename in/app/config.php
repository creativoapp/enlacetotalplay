<?php
date_default_timezone_set('America/Cancun');

//Constants
define('_IMG', '/assets/img/');
define('_CSS', '/assets/css/');
define('_JS', '/assets/js/');
define('_MODEL', $_SERVER['DOCUMENT_ROOT'].'/models/');

//Global Vars
$routeUri = $_SERVER['PHP_SELF'];
$pageUri = basename($_SERVER['SCRIPT_NAME']);

//LoadModels
require(_MODEL.'mCities.php');
require(_MODEL.'mSeo.php');
require(_MODEL.'mBanner.php');
require(_MODEL.'mPromociones.php');

//Cities List
$citiesObj = new CitiesModel();
$cities = $citiesObj->citiesList('mx');