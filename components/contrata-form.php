<form name="formGlobalService" id="contrata-form" method="post" action="" class="columns is-multiline">
    <fieldset class="column is-full">
        <label for="cp-cnt-name">Nombre *</label>
        <input type="text" name="cp-cnt-name" id="cp-cnt-name">
    </fieldset>
    <fieldset class="column is-half">
        <label for="cp-cnt-email">Email *</label>
        <input type="text" name="cp-cnt-email" id="cp-cnt-email">
    </fieldset>
    <fieldset class="column is-half">
        <label for="cp-cnt-phone">Número Telefónico</label>
        <input type="text" name="cp-cnt-phone" id="cp-cnt-phone">
    </fieldset>
    <fieldset class="column is-half">
        <label for="cp-cnt-city">Ciudad</label>
        <select name="cp-cnt-city" id="cp-cnt-city">
            <option value="0">Elige tu ciudad</option>
            <?php
            $cities_list = $citiesObj->citiesList('mx');
            foreach ($cities_list as $key => $city) {
                $selected = '';
                if ($key == $cityUriExtend) {
                    $selected = 'selected';
                }
                echo '<option value="'.$city->text.'" '.$selected.' >'.$city->text.'</option>'."\r\n";
            }
            ?>
        </select>
    </fieldset>
    <fieldset class="column is-half">
        <label for="cp-cnt-cpostal">Código Postal *</label>
        <input type="text" name="cp-cnt-cpostal" id="cp-cnt-cpostal" placeholder="Ej. 77500">
    </fieldset>
    <fieldset class="column is-half">
        <label for="cp-cnt-interesting">Estoy interesado en</label>
        <select name="cp-cnt-interesting" id="cp-cnt-interesting">
            <option value="empresarial">TotalPlay Empresarial</option>
            <?php foreach($packList['tripleplay'] as $paquete) { ?>
                <option value="<?= $paquete['cve'] ?>" <?= (isset($_GET['paquete']) && $paquete['cve'] == $_GET['paquete'] ? 'selected' : '') ?>><?= $paquete['nombre'] ?> (Tripleplay)</option>
            <?php } ?>
            <?php foreach($packList['dobleplay'] as $paquete) { ?>
                <option value="<?= $paquete['cve'] ?>" <?= (isset($_GET['paquete']) && $paquete['cve'] == $_GET['paquete'] ? 'selected' : '') ?>><?= $paquete['nombre'] ?> (Tripleplay)</option>
            <?php } ?>
        </select>
    </fieldset>
    <fieldset class="column is-half">
        <label for="cp-cnt-medio">¿Cómo se entero de nosotros?</label>
        <select name="cp-cnt-medio" id="cp-cnt-medio">
            <option value="0">Elige una opción</option>
            <option value="Facebook">Facebook</option>
            <option value="Google">Google</option>
            <option value="Un amigo">Un amigo</option>
            <option value="Otro">Otro</option>
            <input type="text" name="cp-cnt-medio-other" id="cp-cnt-medio-other" placeholder="Otro Medio">
        </select>
    </fieldset>
    <fieldset class="column is-full">
        <label for="cp-cnt-message">Mensaje</label>
        <textarea name="cp-cnt-message" id="cp-cnt-message" rows="5"></textarea>
    </fieldset>
    <fieldset class="column is-full">
        <input type="submit" id="cp-cnt-button" class="is-button" value="QUIERO CONTRATAR">
    </fieldset>
</form>