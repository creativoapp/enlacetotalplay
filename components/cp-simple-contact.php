<div class="is-contact">
    <span>¡Cámbiate YA!</span>
    <span>Llámanos al <strong>(998) 252 7702</strong></span>
    <form name="cp-call-me" id="simple-contact" class="columns is-multiline is-variable is-1">
        <strong>Nosotros te contáctamos</strong>
        <fieldset class="column is-full">
            <label for="cp-call-name"></label>
            <input type="text" name="cp-call-name" id="cp-call-name" placeholder="Nombre completo *">
        </fieldset>
        <fieldset class="column is-full">
            <label for="cp-call-email"></label>
            <input type="text" name="cp-call-email" id="cp-call-email" placeholder="Email *">
        </fieldset>
        <fieldset class="column is-half">
            <label for="cp-call-phone"></label>
            <input type="text" name="cp-call-phone" id="cp-call-phone" placeholder="Número telefónico">
        </fieldset>
        <fieldset class="column is-half">
            <label for="cp-call-zip"></label>
            <input type="number" name="cp-call-zip" id="cp-call-zip" placeholder="Código Postal *">
        </fieldset>
        <fieldset class="column is-full">
            <input type="submit" id="cp-call-button" class="is-button" value="QUIERO QUE ME CONTACTEN">
        </fieldset>
    </form>
</div>