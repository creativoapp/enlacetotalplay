<?php
class SeoModel {

        
    public function metas($page, $layout, $site = 'mx', $city) {
        
        $metas = array('title' => 'Agencia de Marketing Digital en México | Web Marketing Id', 'description' => '');
        $tags = json_decode(file_get_contents('models/metas-'.$site.'.json'));
        
        //Base
        $uri = explode('.', $page);
        
        $metas['title'] = $tags->{''.$uri[0].''}->title;
        $metas['description'] = $tags->{''.$uri[0].''}->description;

        if($layout != 'default') {
            $metas['title'] = str_replace('{City}', $city['name'], $metas['title']);
            $metas['description'] = str_replace('{City}', $city['name'], $metas['description']);
        }
        
        return $metas;

    }


}
