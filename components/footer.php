    <footer class="is-footer">

        <div class="is-newsletter">
            <div class="container">
                <div class="columns">

                    <div class="column is-two-fifths">
                        <p>¿Quieres conocer nuestras últimas noticias y mejores promociones?</p>
                        <strong>Suscribete a nuestro newsletter</strong>
                    </div>

                    <div class="column is-three-fifths">
                        <div class="is-clearfix is-sub-form">
                            <input type="button" id="cp-newsletter-button" value="SUSCRIBIRME">
                            <input type="text" id="cp-newsletter" placeholder="correo@ejemplo.com">
                        </div>

                        <div class="is-contact">
                            <a href="mailto:info@enlacetotalplay.com" class="is-email"><i class="fas fa-envelope-open-text"></i>info@enlacetotalplay.com</a>
                            <a href="tel:2527702" class="is-phone"><i class="fas fa-fax"></i>(998) 252 7702</a>
                            <div class="dpy-inl">
                                <a href="https://www.facebook.com/TotalPlayCancunMx/" target="_blank" class="is-social is-facebook" title="Síguenos en Facebook"><i class="fab fa-facebook"></i></a>
                                <a href="https://twitter.com/ETotalplay" target="_blank" class="is-social is-twitter" title="Síguenos en Twitter"><i class="fab fa-twitter"></i></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="container">
            <div class="columns is-multiline">

                <div class="column is-full">
                    <ul class="column is-full is-clearfix is-nav">
                        <li><a href="/internet">Internet</a></li>
                        <li><a href="/television">Televisión</a></li>
                        <li><a href="/telefonia">Telefonía</a></li>
                        <li><a href="/paquetes">Paquetes</a></li>
                        <li><a href="/paquetes">Canales</a></li>
                        <li><a href="/promociones">Promociones</a></li>
                        <li><a href="/empresarial">Empresarial</a></li>
                        <li><a href="/contrata">Contrata</a></li>
                        <li><a href="/contacto">Contacto</a></li>
                    </ul>
                </div>

                <div class="column is-full is-cities">
                    <ul class="is-clearfix">
                        <?php foreach ($cities as $city) { ?>
                            <li><a href="/totalplay-<?= $city->{'url'}; ?>" title="Totalplay en <?= $city->{'text'}; ?>"><?= $city->{'text'}; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>

            </div>
        </div>
    </footer>

    <!--GOOGLE ANALYTICS-->
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-22304550-28']);
        _gaq.push(['_trackPageview']);
        (function() {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();
    </script>

    <!--CHAT CODE-->
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var $_Tawk_API = {},
            $_Tawk_LoadStart = new Date();
        (function() {
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/563d72b8b2ea3bb02546985a/default';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
    </script>
    <!--End of Tawk.to Script-->

    <script type="text/javascript" src="<?= _JS . 'jquery-1.9.0.min.js'; ?>"></script>
    <script type="text/javascript" src="<?= _JS . 'components.js'; ?>"></script>
    <script type="text/javascript" src="<?= _JS . 'tp.js'; ?>"></script>

    </body>

    </html>