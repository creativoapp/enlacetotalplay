<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view is-view-channels">
    <div class="container">
        <div class="columns is-multiline">
        
            <div class="column">
                <h1 class="is-title-home">Canales Total<span class="cl-p">p</span><span class="cl-l">l</span><span class="cl-a">a</span><span class="cl-y">y</span></h1>
                
                <div class="is-tabs is-clearfix">
                    <h2><a href="#tvhd" class="is-selected"><img src="/assets/img/favicon.ico"> 80 Canales, 35 HD</a></h2>
                    <h2><a href="#tvbasic"><img src="/assets/img/favicon.ico"> TV Básica</a></h2>
                    <h2><a href="#tvadvance"><img src="/assets/img/favicon.ico"> TV Avanzada</a></h2>
                    <h2><a href="#tvpremium"><img src="/assets/img/favicon.ico"> TV Premium</a></h2>
                </div>

                <div class="is-tabs-content">
                    <div id="tvhd" class="is-show">
                        <h2 class="is-size-5">El plan de TV <strong>80 Canales, 35 HD</strong> incluye los siguientes canales:</h2>
                        <img src="/assets/img/tv-canales-totalplay.jpg">
                    </div>
                    <div id="tvbasic">
                        <h2 class="is-size-5">El plan de TV <strong>TV Básica</strong> incluye los siguientes canales:</h2>
                        <img src="/assets/img/tv-basica-totalplay.jpg">
                    </div>
                    <div id="tvadvance">
                        <h2 class="is-size-5">El plan de TV <strong>TV Avanzada</strong> incluye los siguientes canales:</h2>
                        <img src="/assets/img/tv-avanzada-totalplay.jpg">
                    </div>
                    <div id="tvpremium">
                        <h2 class="is-size-5">El plan de TV <strong>TV Premium</strong> incluye los siguientes canales:</h2>
                        <img src="/assets/img/tv-premium-totalplay.jpg">
                    </div>
                </div>
                
            </div>

            <aside class="column is-one-quarter">
                <?php require('components/cp-simple-contact.php'); ?>
            </aside>

        </div>
    </div>
</section>

<?php require __DIR__ . '/components/footer.php'; ?>