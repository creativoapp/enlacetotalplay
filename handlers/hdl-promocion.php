<?php
if (isset($_POST['action'])) {
    include($_SERVER['DOCUMENT_ROOT'] . '/models/mPromociones.php');
    $mPromociones = new Promociones();
    if ($_POST['action'] == 'create' || $_POST['action'] == 'edit') {
        $data = [
            'promocionImg' => $_POST['promocionImg-name'],
            'shortName' => trim($_POST['shortName']),
            'promocionTitulo' => trim($_POST['promocionTitulo']),
            'promocioncve' => $_POST['promocioncve'],
            'promocionDescription' => $_POST['promocionDescription'],
            'promocionVigencia' => $_POST['promocionVigencia'],
            'promocionCity' => $_POST['promocionCity']
        ];
    }
    if ($_POST['action'] == 'create') {
        $data['promocionImg'] = uploadImage($_POST['action']);
        if ($data['promocionCity'] == '') {
            $data['promocionType'] = 'G';
        } else {
            $data['promocionType'] = 'C';
        }
        $valid = $mPromociones->validatePromocion($data['shortName'], $data['promocioncve']);
        if ($valid['name'] == false ||$valid['cve'] == false) {
            echo json_encode($valid);
        } else {
            echo $mPromociones->createPromocion($data);
        }
    }
    if ($_POST['action'] == 'edit') {
        $data['promocionImg'] = uploadImage($_POST['action']);
        if ($data['promocionCity'] != $_POST['last-city']) {
            if ($data['promocionCity'] == '') {
                $data['promocionType'] = 'G';
            } else {
                $data['promocionType'] = 'C';
            }
        }
        $valid = $mPromociones->validatePromocion($data['shortName'], $data['promocioncve'], $_POST['idpromocion']);
        if ($valid['name'] == false ||$valid['cve'] == false) {
            echo json_encode($valid);
        } else {
            echo $mPromociones->editPromocion($_POST['idpromocion'], $data);
        }
    }
    if ($_POST['action'] == 'delete') {
        $file = $_SERVER['DOCUMENT_ROOT'] . '/assets/img/promociones/' . $_POST['promocionImg'];
        if (file_exists($file)) {
            unlink($file);
        }
        echo $mPromociones->deletePromocion($_POST['idpromocion']);
    }
}

function uploadImage($action)
{
    $dir_subida = $_SERVER['DOCUMENT_ROOT'] . '/assets/img/promociones/';
    if ($action == 'create') {
        return upImg($dir_subida);
    }
    if ($action == 'edit') {
        if (trim($_FILES['promocionImg']['name']) != '') {
            if (file_exists($dir_subida . $_POST['promocionImg-name'])) {
                unlink($dir_subida . $_POST['promocionImg-name']);
            }
            return upImg($dir_subida);
        } else {
            return $_POST['promocionImg-name'];
        }
    }
}

function upImg($dir_subida)
{
    $_FILES['promocionImg']['name'] = clearImgName($_FILES['promocionImg']['name']);
    $fichero_subido = $dir_subida . basename($_FILES['promocionImg']['name']);
    if (file_exists($fichero_subido)) {
        $exp = explode('.', $_FILES['promocionImg']['name']);
        $limexp = count($exp) - 1;
        $_FILES['promocionImg']['name'] = $exp[0] . '_' . time() . '.' . $exp[$limexp];
        $fichero_subido = $dir_subida . basename($_FILES['promocionImg']['name']);
    }
    if (move_uploaded_file($_FILES['promocionImg']['tmp_name'], $fichero_subido)) {
        return trim(basename($_FILES['promocionImg']['name']));
    } else {
        return '';
    }
}

function clearImgName($name)
{
    $name = str_replace(' ', '_', $name);
    $name = str_replace('á', 'a', $name);
    $name = str_replace('é', 'e', $name);
    $name = str_replace('í', 'i', $name);
    $name = str_replace('ó', 'o', $name);
    $name = str_replace('ú', 'u', $name);
    $name = str_replace('Á', 'A', $name);
    $name = str_replace('É', 'E', $name);
    $name = str_replace('Í', 'I', $name);
    $name = str_replace('Ó', 'O', $name);
    $name = str_replace('Ú', 'U', $name);
    $name = str_replace('Ñ', 'N', $name);
    $name = str_replace('ñ', 'n', $name);
    $name = preg_replace('([^A-Za-z0-9 ._-])', '', $name);
    return $name;
}
