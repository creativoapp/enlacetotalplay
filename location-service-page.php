<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view-servicedetail is-view-servicelocationdetail">
    
    <div class="is-header-service">
        <div class="container">
            <div class="columns">

                <div class="column is-two-thirds">
                    <h1>{ Service } en <span>{ City }</span></h1>
                    <div class="is-bread">
                        <a href="/">Web Marketing ID</a>
                        <i class="fas fa-arrow-right"></i>
                        <a href="/ubicaciones">Ubicaciones</a>
                        <i class="fas fa-arrow-right"></i>
                        <a href="/location-page">{ City }</a>
                        <i class="fas fa-arrow-right"></i>
                        <span>{ Service }</span>
                    </div>

                    <p class="is-pr-big">El Posicionamiento Web o SEO (Search Engine Optimization) es una actividad de Marketing Digital en internet que lleva a una Página Web a mejorar su posición en los Resultados de búsqueda en la Web.</p>
                </div>
                <div class="column is-one-third">
                    <img src="<?=_IMG.'seo-ilustracion.png';?>" class="is-img-big">
                </div>

            </div>
        </div>
    </div>
    
    <div class="container">
        <div class="columns is-multiline">

            <div class="column is-one-third">
                <img src="<?=_IMG.'mac-book.png';?>">
            </div>

            <div class="column is-two-thirds">
                <h2>Extender info sobre seo</h2>
                <p class="is-pr-medium">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                <p class="is-pr-medium">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.</p>
            </div>

            <div class="column is-half is-listed">
                <h3>Sub-temas de Seo</h3>

                <p class="is-pr-medium">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>

                <div class="is-item">
                    <h4><i class="fas fa-ad"></i> Keyword Planner</h4>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                </div>

                <div class="is-item">
                    <h4><i class="fas fa-link"></i> Linkbuilding</h4>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                </div>

                <div class="is-item">
                    <h4><i class="fab fa-wordpress-simple"></i> Articulos Blog</h4>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                </div>

                <div class="is-item">
                    <h4><i class="fas fa-anchor"></i> Otros</h4>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                </div>

            </div>

            <div class="column is-half is-listed">
                <div class="is-green">
                    <h3>Posicionamiento Web en México</h3>

                    <ul class="is-clearfix">
                        <?php include('components/cities-list.php'); ?>
                    </ul>

                </div>


                <h4>Me interesa este servicio</h4>
                <?php include('components/form-service.php'); ?>
            </div>

        </div>
    </div>


    <div class="is-steps">
        <div class="container">
            <div class="columns">
            
                <div class="column is-one-quarter">
                    <h4>1. Analizar marca</h4>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                </div>

                <div class="column is-one-quarter">
                    <h4>2. Estrategia</h4>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                </div>

                <div class="column is-one-quarter">
                    <h4>3. Lanzamiento de campañas</h4>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                </div>

                <div class="column is-one-quarter">
                    <h4>4. Analisis de resultados</h4>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                </div>

            </div>
        </div>
    </div>
    
</section>

<?php require __DIR__ . '/components/footer.php'; ?>