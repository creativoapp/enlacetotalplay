$(document).ready(function(){
    var is_hidde = false;
    $('#btn-menu').on('click', function(e) {
        e.preventDefault();
        if (is_hidde == false) {
            $('.is-header .is-menu').addClass('show').removeClass('hidden');
            $(this).children('i').removeClass('fa-bars').addClass('fa-times');
            is_hidde = true;
        } else if (is_hidde == true) {
            $('.is-header .is-menu').removeClass('show').addClass('hidden');
            $(this).children('i').removeClass('fa-times').addClass('fa-bars');
            is_hidde = false;
        }
    });
});