<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view is-view-deals">
    <div class="container">
        <div class="columns is-multiline">

            <div class="column">
                <h1 class="is-title-home">Promociones Total<span class="cl-p">p</span><span class="cl-l">l</span><span class="cl-a">a</span><span class="cl-y">y</span></h1>

                <?php foreach ($promList as $promocion) { ?>
                    <div class="columns is-row-deal">
                        <div class="column is-one-third">
                            <img src="<?= _IMG . 'promociones/' . $promocion['promocionImg'] ?>">
                        </div>
                        <div class="column">
                            <h2 class="h-mb-0 is-size-5"><?= $promocion['shortName'] ?> | <?= $month ?></h2>
                            <h3 class="is-size-6"><?= $promocion['promocionTitulo'] ?></h3>
                            <h4 class="is-size-6">Vigencia: <?= date_format(date_create($promocion['promocionVigencia']), 'd/m/Y') ?></h4>

                            <p class="has-text-justified"><?= $promocion['promocionDescription'] ?></p>
                            <a href="/contacto?promo=<?= $promocion['promocioncve'] ?>">
                                <h5 class="h-mb-0 is-size-7">Validar promoción</h5>
                            </a>
                        </div>
                    </div>
                <?php } ?>

                <!--<div class="columns is-row-deal">
                    <div class="column is-one-third">
                        <img src="/assets/img/totalplay-promociones.jpg">
                    </div>
                    <div class="column">
                        <h2 class="h-mb-0">Mas por Menos | Agosto</h2>
                        <h3>Obten Más Veneficios por mucho Menos</h3>
                        <h4>Vigencia: 25/08/2020</h4>

                        <p class="has-text-justified">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.</p>
                        <a href="/contacto?promo=1">
                            <h5 class="h-mb-0">Validar promoción</h5>
                        </a>
                    </div>
                </div>-->

                <!--<div class="columns is-row-deal">
                    <div class="column is-one-third">
                        <img src="/assets/img/totalplay-promociones.jpg">
                    </div>
                    <div class="column">
                        <h3>Promoción Mes</h3>
                        <h2>Titulo de la Promoción</h2>

                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.</p>
                    </div>
                </div>

                <div class="columns is-row-deal">
                    <div class="column is-one-third">
                        <img src="/assets/img/totalplay-promociones.jpg">
                    </div>
                    <div class="column">
                        <h3>Promoción Mes</h3>
                        <h2>Titulo de la Promoción</h2>

                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.</p>
                    </div>
                </div>-->

            </div>

            <aside class="column is-one-quarter">
                <?php require('components/cp-simple-contact.php'); ?>
            </aside>

        </div>
    </div>
</section>

<?php require __DIR__ . '/components/footer.php'; ?>