<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view is-view-deals">
    <div class="container">
        <div class="columns is-multiline">

            <div class="column">
                <h1 class="is-title-home">Promociones Total<span class="cl-p">p</span><span class="cl-l">l</span><span class="cl-a">a</span><span class="cl-y">y</span></br>en <?= $city_page['name'] ?></h1>

                <?php foreach ($promList as $promocion) { ?>
                    <div class="columns is-row-deal">
                        <div class="column is-one-third">
                            <img src="<?= _IMG . 'promociones/' . $promocion['promocionImg'] ?>">
                        </div>
                        <div class="column">
                            <h2 class="h-mb-0 is-size-5"><?= $promocion['shortName'] ?> | <?= $month ?></h2>
                            <h3 class="is-size-6"><?= $promocion['promocionTitulo'] ?></h3>
                            <h4 class="is-size-6">Vigencia: <?= date_format(date_create($promocion['promocionVigencia']), 'd/m/Y') ?></h4>

                            <p class="has-text-justified"><?= $promocion['promocionDescription'] ?></p>
                            <a href="/contacto?promo=<?= $promocion['promocioncve'] ?>">
                                <h5 class="h-mb-0 is-size-7">Validar promoción</h5>
                            </a>
                        </div>
                    </div>
                <?php } ?>

            </div>

            <aside class="column is-one-quarter">
                <?php require('components/cp-simple-contact.php'); ?>
            </aside>

        </div>
    </div>
</section>

<?php require __DIR__ . '/components/footer.php'; ?>