<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view is-view-contact">
    <div class="container">
        <div class="columns is-multiline">

            <div class="column is-full">
                <h1 class="is-title-home">Contratar Total<span class="cl-p">p</span><span class="cl-l">l</span><span class="cl-a">a</span><span class="cl-y">y</span></br>desde <?= $city_page['name'] ?></h1>
                <p class="is-pr-big">Completa el siguiente formulario de contacto y uno de nuestros asesores Totalplay se comunicará contigo a la brevedad para continuar con el proceso de contratación.</p>
            </div>

        </div>
    </div>

    <br><br>
    <div class="container">
        <div class="columns is-variable is-5">

            <div class="column is-half">
                <?php include($_SERVER['DOCUMENT_ROOT'].'/components/contrata-form.php') ?>
            </div>

            <div class="column is-half">

                <p class="is-pr-medium">También puede iniciar tu proceso de contratación con alguna de las siguientes formas:</p>

                <div class="is-item-contact">
                    <span><i class="fas fa-map-signs"></i> Visítanos en</span>
                    <strong>Av. La Luna, SM 48, MZ 9, Lote 4, N 4B<br>Las Palmas, 77506<br> Cancún, Quintana Roo</strong>
                </div>

                <div class="is-item-contact">
                    <span><i class="fab fa-whatsapp"></i> Escríbenos al Whatsapp</span>
                    <strong>(998) 252 7702</strong>
                </div>

                <div class="is-item-contact">
                    <span><i class="fas fa-envelope-open-text" aria-hidden="true"></i> Escríbenos a</span>
                    <strong>info@enlacetotalplay.com</strong>
                </div>

                <div class="is-item-contact">
                    <span><i class="fas fa-fax" aria-hidden="true"></i> Llámanos al</span>
                    <strong>(998) 252 7702</strong>
                </div>


            </div>

        </div>
    </div>

    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d29768.522326874852!2d-86.86555338201413!3d21.14979979969175!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa9c2274b427b6804!2sEnlace%20Totalplay!5e0!3m2!1ses!2smx!4v1595693111321!5m2!1ses!2smx" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

</section>

<?php require __DIR__ . '/components/footer.php'; ?>