$(document).ready(function(){

    //Paquetes
    $('.is-doble-pack, .is-text-dobleplay').hide();

    $('.is-custom-switch span, .is-custom-switch h2.button-pkg').on('click', function(){
        $('.is-custom-switch span, .is-custom-switch h2.button-pkg').removeClass('is-selected');
        $(this).addClass('is-selected');

        var package = $(this).data('val');
        var text = $(this).data('text');

        //Packges
        $('.is-packages .is-one-quarter').stop(true).fadeOut('last');
        $('.is-packages .'+package+'').stop(true).fadeIn('last');

        //Text
        $('.is-text-packages').stop(true).fadeOut('last');
        $('#'+text+'').stop(true).fadeIn('last');

    });


    //Canales
    $('.is-tabs-content div').hide();
    $('.is-tabs-content .is-show').show();

    $('.is-tabs a').on('click', function(event){
        event.preventDefault();

        var channels = $(this).attr('href');

        $('.is-tabs a').removeClass('is-selected');
        $(this).addClass('is-selected');
        $('.is-tabs-content div').stop(true).fadeOut('last');
        $('.is-tabs-content '+channels+'').stop(true).fadeIn('last');
    });

});