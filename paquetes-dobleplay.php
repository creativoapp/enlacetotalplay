<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view is-view-packages">
    <div class="container">
        <div class="columns is-multiline">

            <div class="column is-full">
                <h1 class="is-title-home">Paquete Doble<span class="cl-p">p</span><span class="cl-l">l</span><span class="cl-a">a</span><span class="cl-y">y</span></h1>
                <p class="is-text is-text-packages is-text-dobleplay" id="is-text-dobleplay">A partir del plan Emociónate+, disfruta por 30 días de un decodificador 4K con 275 canales y un punto de acceso que te permitirá usar tus aplicaciones favoritas.</p>

            </div>

            <div class="column is-full is-packages">
                <div class="columns is-multiline">

                    <!--DOBLEPLAY-->
                    <?php foreach ($packList['dobleplay'] as $dobleplay) { ?>
                        <div class="column is-one-quarter">
                            <div class="is-item is-default <?= $dobleplay['color'] ?>">
                                <div class="is-title">
                                    <strong><?= $dobleplay['nombre'] ?></strong>
                                    <span class="is-megas is-size-4">Internet de <?= $dobleplay['megas'] ?><small>Megas</small></span>
                                </div>
                                <div class="is-complements is-clearfix is-flex-div">
                                    <span class="<?= ($dobleplay['canales'] == 0) ? 'is-hidden' : '' ?>">
                                        <i class="fas fa-tv"></i>
                                        <small><strong><?= $dobleplay['canales'] ?></strong> Canales</small>
                                        <small class="is-tag"><?= $dobleplay['tvs'] ?> TV</small>
                                    </span>
                                    <span class="<?= ($dobleplay['lineas'] == 0) ? 'is-hidden' : '' ?>">
                                        <i class="fas fa-fax"></i>
                                        <small><strong><?= $dobleplay['lineas'] ?></strong> Línea(s) telefónica(s)</small>
                                    </span>
                                </div>
                                <div class="is-discount">
                                    <strong class="is-size-4"><small>$</small><?= $dobleplay['descuentomxn'] ?><small class="is-size-7 dpy-inl">MXN</small><span class="dpy-inl is-size-6"> de Descuento</span></strong>
                                    <span class="is-size-6">a partir del <?= $dobleplay['mesdescuento'] ?>° MES</span>
                                </div>
                                <div class="is-total">
                                    <span>Pronto Pago</span>
                                    <strong>$<?= number_format($dobleplay['prontopago'], 0, '.', ',') ?><small class="dpy-inl">MXN</small></strong>
                                    <span>Precio de Lista</span>
                                    <strong>$<?= number_format($dobleplay['listapago'], 0, '.', ',') ?><small class="dpy-inl">MXN</small></strong>
                                    <a href="/contrata?paquete=<?= $dobleplay['cve'] ?>" class="btn-paquete">Contratar</a>
                                    <small>*Consulta términos y condiciones</small>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    

                </div>
            </div>

        </div>
    </div>
</section>

<?php require __DIR__ . '/components/footer.php'; ?>