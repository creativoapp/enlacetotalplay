<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view is-view-service">

    <!--OVERVIEW-->
    <div class="container">
        <div class="columns">
            
            <div class="column is-half is-overview">
                <h1 class="is-title-home">Telefonía Total<span class="cl-p">p</span><span class="cl-l">l</span><span class="cl-a">a</span><span class="cl-y">y</span></h1>
                <p class="is-pr-big has-text-justified">Mantente siempre comunicado con llamadas a números fijos y Larga Distancia Internacional sin límite, además de contar con los minutos a celular que justo necesitas. ¡Ahorra y habla mucho más con TotalPlay!</p>

            
                <div class="is-clearfix">
                    <div class="is-item is-item-phone">
                        <i class="fas fa-check-circle"></i>
                        <h2 class="is-size-6 dpy-inl">Larga distancia</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                    <div class="is-item is-item-phone">
                        <i class="fas fa-check-circle"></i>
                        <h2 class="is-size-6 dpy-inl">Todo México sin costo</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                    <div class="is-item is-item-phone">
                        <i class="fas fa-check-circle"></i>
                        <h2 class="is-size-6 dpy-inl">Europa accesible</h2>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                </div>

            </div>

            <div class="column is-half is-overview dv-center-content">
                <img src="/assets/img/telefonia-totalplay.png" class="is-img-centered">
            </div>

        </div>
    </div>

    <div class="is-row-service">
        <div class="container">
            <div class="columns is-multiline">
            
                <div class="column is-one-third is-item is-item-phone">
                    <h2><i class="fas fa-map-marker-alt"></i>Portabilidad</h2>
                    <p>Conserva tu número telefónico aunque cambies de compañía y sigue en contacto con tus familiares y amigos. El tramite de portabilidad que ofrece TotalPlay es muy fácil ya que se realiza de manera electrónica y sin largos procesos.</p>
                </div>

                <div class="column is-one-third is-item is-item-phone">
                    <h2><i class="fas fa-tv"></i>Números fijos y larga distancia</h2>
                    <p>Llama a todos tus familiares sin costo extra y sin importar donde se encuentren. Disfruta de llamadas a números fijos y números de Larga Distancia Internacional <small>(EUA y Canadá)</small> sin límite en cada uno de los planes que TotalPlay tiene para ti...</p>
                </div>

                <div class="column is-one-third is-item is-item-phone">
                    <h2><i class="fas fa-laptop"></i>Minutos a Celular</h2>
                    <p>Con TotalPlay Te incluimos minutos para llamadas a celular nacional de cualquier compañía. Además los minutos adicionales a celular cuestan sólo 84 centavos, somos la mejor opción.</p>
                </div>

            </div>
        </div>
    </div>

</section>

<?php require __DIR__ . '/components/footer.php'; ?>