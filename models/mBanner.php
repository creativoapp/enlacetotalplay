<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/models/BD.php');

class Banner extends BD
{

    public function getBanner($ciudad = null)
    {
        $bd = $this->openBD();
        if ($ciudad == null) {
            $sql = $bd->prepare("SELECT * FROM banner WHERE bannerCity = '' ORDER BY bannerOrden ASC;");
        } else {
            $sql = $bd->prepare("SELECT * FROM banner WHERE bannerCity IN (:city, '') ORDER BY bannerCity DESC, bannerOrden ASC;");
            $sql->bindParam('city', $ciudad);
        }
        $sql->execute();
        $rt = [];
        if ($sql->rowCount() > 0) {
            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                array_push($rt, [
                    'id' => $row['idbanner'],
                    'img' => $row['bannerSrc'],
                    'alt' => $row['bannerAlt'],
                    'title' => $row['bannerTitle'],
                    'order' => $row['bannerOrden'],
                    'city' => $row['bannerCity']
                ]);
            }
        }
        $this->closeBD($bd);
        return $rt;
    }

    public function getBannerbyId($id)
    {
        $bd = $this->openBD();
        $sql = $bd->prepare("SELECT * FROM banner WHERE idbanner = :id");
        $sql->bindParam('id', $id);
        $sql->execute();
        $rt = [];
        if ($sql->rowCount() > 0) {
            $row = $sql->fetch(PDO::FETCH_ASSOC);
            $rt = $row;
        }
        $this->closeBD($bd);
        return $rt;
    }

    public function validOrder($orden, $ciudad)
    {
        $bd = $this->openBD();
        $sql = $bd->prepare("SELECT bannerOrden FROM banner WHERE bannerCity = :city AND bannerOrden = :order");
        $sql->bindParam('city', $ciudad);
        $sql->bindParam('order', $orden);
        $sql->execute();
        if ($sql->rowCount() > 0) {
            return false;
        } else {
            return true;
        }
        $this->closeBD($bd);
    }

    public function lastOrder($ciudad)
    {
        $bd = $this->openBD();
        $sql = $bd->prepare("SELECT bannerOrden FROM banner WHERE bannerCity = :city ORDER BY bannerOrden DESC LIMIT 1;");
        $sql->bindParam('city', $ciudad);
        $sql->execute();
        $rt = 1;
        if ($sql->rowCount() > 0) {
            $row = $sql->fetch(PDO::FETCH_ASSOC);
            $rt = ($row['bannerOrden'] + 1);
        }
        $this->closeBD($bd);
        return $rt;
    }

    public function createBanner($data)
    {
        $bd = $this->openBD();
        $sql = $bd->prepare("INSERT INTO banner (bannerSrc, bannerAlt, bannerTitle, bannerCity, bannerOrden) VALUES (:src, :alt, :title, :city, :order);");
        $sql->bindParam('src', $data['bannerSrc']);
        $sql->bindParam('alt', $data['bannerAlt']);
        $sql->bindParam('title', $data['bannerTitle']);
        $sql->bindParam('city', $data['bannerCity']);
        $sql->bindParam('order', $data['bannerOrden']);
        $sql->execute();
        if ($sql->rowCount() > 0) {
            return json_encode(['status' => 'done', 'msg' => 'Banner Agregado']);
        } else {
            return json_encode(['status' => 'fail', 'msg' => 'Intentelo más tarde.']);
        }
        $this->closeBD($bd);
    }

    public function editBanner($id, $data)
    {
        $bd = $this->openBD();
        if (isset($data['bannerOrden'])) {
            $sql = $bd->prepare("UPDATE banner SET bannerSrc = :src, bannerAlt = :alt, bannerTitle = :title, bannerCity = :city, bannerOrden = :order WHERE idbanner = :id;");
            $sql->bindParam('order', $data['bannerOrden']);
        } else {
            $sql = $bd->prepare("UPDATE banner SET bannerSrc = :src, bannerAlt = :alt, bannerTitle = :title, bannerCity = :city WHERE idbanner = :id;");
        }
        $sql->bindParam('src', $data['bannerSrc']);
        $sql->bindParam('alt', $data['bannerAlt']);
        $sql->bindParam('title', $data['bannerTitle']);
        $sql->bindParam('city', $data['bannerCity']);
        $sql->bindParam('id', $id);
        $sql->execute();
        if ($sql->rowCount() > 0) {
            return json_encode(['status' => 'done', 'msg' => 'Banner Modificado']);
        } else {
            return json_encode(['status' => 'fail', 'msg' => 'No se realizaron Cambios.']);
        }
        $this->closeBD($bd);
    }

    public function editOrden($id, $orden)
    {
        $bd = $this->openBD();
        $sql = $bd->prepare("UPDATE banner SET bannerOrden = :order WHERE idbanner = :id;");
            $sql->bindParam('order', $orden);
        $sql->bindParam('id', $id);
        $sql->execute();
        if ($sql->rowCount() > 0) {
            return json_encode(['status' => 'done', 'msg' => 'Banner Modificado']);
        } else {
            return json_encode(['status' => 'fail', 'msg' => 'No se realizaron Cambios.']);
        }
        $this->closeBD($bd);
    }

    public function deleteBanner($id)
    {
        $bd = $this->openBD();
        $sql = $bd->prepare("DELETE FROM banner WHERE idbanner = :id;");
        $sql->bindParam('id', $id);
        $sql->execute();
        if ($sql->rowCount() > 0) {
            return json_encode(['status' => 'done', 'msg' => 'Banner Eliminado']);
        } else {
            return json_encode(['status' => 'fail', 'msg' => 'Intentelo más tarde.']);
        }
        $this->closeBD($bd);
    }
}
