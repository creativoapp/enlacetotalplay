<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/models/BD.php');

class Promociones extends BD
{

    public function getPromociones($ciudad = null, $valid_vigency = true)
    {
        $bd = $this->openBD();
        if ($valid_vigency == true) {
            $date = date('Y-m-d');
            if ($ciudad == null) {
                $sql = $bd->prepare("SELECT * FROM promociones WHERE promocionType = 'G' AND promocionVigencia > :dt ORDER BY promocionVigencia ASC;");
            } else {
                $sql = $bd->prepare("SELECT * FROM promociones WHERE promocionCity IN (:city, '') AND promocionVigencia > :dt ORDER BY promocionCity DESC, promocionVigencia ASC;");
                $sql->bindParam('city', $ciudad);
            }
            $sql->bindParam('dt', $date);
        } else {
            if ($ciudad == null) {
                $sql = $bd->prepare("SELECT * FROM promociones WHERE promocionType = 'G' ORDER BY promocionVigencia ASC;");
            } else {
                $sql = $bd->prepare("SELECT * FROM promociones WHERE promocionCity IN (:city, '') ORDER BY promocionCity DESC, promocionVigencia ASC;");
                $sql->bindParam('city', $ciudad);
            }
        }
        $sql->execute();
        $rt = [];
        if ($sql->rowCount() > 0) {
            while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
                array_push($rt, $row);
            }
        }
        $this->closeBD($bd);
        return $rt;
    }

    public function getPromocion($cve)
    {
        $bd = $this->openBD();
        $sql = $bd->prepare("SELECT * FROM promociones WHERE promocioncve = :cve");
        $sql->bindParam('cve', $cve);
        $sql->execute();
        $rt = [];
        if ($sql->rowCount() > 0) {
            $rt = $sql->fetch(PDO::FETCH_ASSOC);
        }
        $this->closeBD($bd);
        return $rt;
    }

    public function validatePromocion($name, $cve, $id = null)
    {
        $bd = $this->openBD();
        $rt = ['status' => 'validation', 'name' => true, 'cve' => true];
        //Validar Nombre
        if ($id != null) {
            $sqlName = $bd->prepare("SELECT * FROM promociones WHERE shortName = :nm AND idpromocion NOT IN (:id)");
            $sqlName->bindParam('id', $id);
        } else {
            $sqlName = $bd->prepare("SELECT * FROM promociones WHERE shortName = :nm");
        }
        $sqlName->bindParam('nm', $name);
        $sqlName->execute();
        if ($sqlName->rowCount() > 0) {
            $rt['name'] = false;
        }
        //Validar CVE
        if ($id != null) {
            $sqlCve = $bd->prepare("SELECT * FROM promociones WHERE promocioncve = :cve AND idpromocion NOT IN (:id)");
            $sqlCve->bindParam('id', $id);
        } else {
            $sqlCve = $bd->prepare("SELECT * FROM promociones WHERE promocioncve = :cve");
        }
        $sqlCve->bindParam('cve', $cve);
        $sqlCve->execute();
        if ($sqlCve->rowCount() > 0) {
            $rt['cve'] = false;
        }
        return $rt;
        $this->closeBD($bd);
    }

    public function createPromocion($data)
    {
        $bd = $this->openBD();
        $sql = $bd->prepare("INSERT INTO promociones (promocioncve, shortName, promocionTitulo, promocionImg, promocionDescription, promocionVigencia, promocionType, promocionCity) VALUES (:cve, :nme, :ttl, :img, :dsc, :vig, :tpe, :cty);");
        $sql->bindParam('cve', $data['promocioncve']);
        $sql->bindParam('nme', $data['shortName']);
        $sql->bindParam('ttl', $data['promocionTitulo']);
        $sql->bindParam('img', $data['promocionImg']);
        $sql->bindParam('dsc', $data['promocionDescription']);
        $sql->bindParam('vig', $data['promocionVigencia']);
        $sql->bindParam('tpe', $data['promocionType']);
        $sql->bindParam('cty', $data['promocionCity']);
        $sql->execute();
        if ($sql->rowCount() > 0) {
            return json_encode(['status' => 'done', 'msg' => 'Promoción Agregada']);
        } else {
            return json_encode(['status' => 'fail', 'msg' => 'Intentelo más tarde.']);
        }
        $this->closeBD($bd);
    }

    public function editPromocion($id, $data)
    {
        $bd = $this->openBD();
        if (isset($data['promocionType'])) {
            $sql = $bd->prepare("UPDATE promociones SET promocioncve = :cve, shortName = :nme, promocionTitulo = :ttl, promocionImg = :img, promocionDescription = :dsc, promocionVigencia = :vig, promocionType = :tpe, promocionCity = :cty WHERE idpromocion = :id;");
            $sql->bindParam('tpe', $data['promocionType']);
        } else {
            $sql = $bd->prepare("UPDATE promociones SET promocioncve = :cve, shortName = :nme, promocionTitulo = :ttl, promocionImg = :img, promocionDescription = :dsc, promocionVigencia = :vig, promocionCity = :cty WHERE idpromocion = :id;");
        }
        $sql->bindParam('cve', $data['promocioncve']);
        $sql->bindParam('nme', $data['shortName']);
        $sql->bindParam('ttl', $data['promocionTitulo']);
        $sql->bindParam('img', $data['promocionImg']);
        $sql->bindParam('dsc', $data['promocionDescription']);
        $sql->bindParam('vig', $data['promocionVigencia']);
        $sql->bindParam('cty', $data['promocionCity']);
        $sql->bindParam('id', $id);
        $sql->execute();
        if ($sql->rowCount() > 0) {
            return json_encode(['status' => 'done', 'msg' => 'Promoción Modificada']);
        } else {
            return json_encode(['status' => 'fail', 'msg' => 'No se realizaron cambios.']);
        }
        $this->closeBD($bd);
    }

    public function deletePromocion($id)
    {
        $bd = $this->openBD();
        $sql = $bd->prepare("DELETE FROM promociones WHERE idpromocion = :id;");
        $sql->bindParam('id', $id);
        $sql->execute();
        if ($sql->rowCount() > 0) {
            return json_encode(['status' => 'done', 'msg' => 'Promoción Eliminada']);
        } else {
            return json_encode(['status' => 'fail', 'msg' => 'Intentelo más tarde.']);
        }
        $this->closeBD($bd);
    }
}
