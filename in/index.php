<?php include('app/config.php') ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="<?= _CSS . 'bulma.min.css'; ?>">
    <link rel="stylesheet" href="<?= _CSS . 'components.min.css'; ?>">
    <link rel="stylesheet" href="<?= _CSS . 'dashboard.min.css'; ?>">

    <link rel="shortcut icon" type="image/png" href="<?= _IMG . 'favicon.ico'; ?>" />

    <script src="https://kit.fontawesome.com/94233a6903.js" crossorigin="anonymous"></script>

    <title>Login</title>
</head>

<body>
    <div class="columns col-top-0 login">
        <div class="column is-half is-background-log hidden-mobile"></div>
        <div class="column is-half is-con-center hf-mobile">
            <form action="banners" id="login-form" method="POST">
                <fieldset>
                    <label for="user">Nombre de Usuario</label>
                    <input type="text" name="user" placeholder="Nombre de Usuario">
                </fieldset>
                <fieldset>
                    <label for="pass">Contraseña</label>
                    <input type="password" name="pass" placeholder="********">
                </fieldset>
                <fieldset>
                    <input type="submit" class="is-button" value="Entrar">
                </fieldset>
            </form>
        </div>
    </div>
</body>

</html>