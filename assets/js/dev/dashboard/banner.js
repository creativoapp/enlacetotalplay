$(document).ready(function () {
    var app = {
        'path': window.location.protocol + '//' + window.location.hostname + '/',
        'isvalid': true
    };
    $('#frm-banner').on('submit', function (e) {
        e.preventDefault();
        var val = true;
        if ($('#action').val() == 'create' && $('#bannerSrc').val() == 0) {
            val = false;
        }
        if (app.isvalid == false) {
            val = false;
        }
        if (val == true) {
            var data = new FormData(this);
            $.ajax({
                data: data,
                url: app.path + 'handlers/hdl-banner.php',
                type: 'POST',
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    var res = eval('(' + data + ')');
                    alert(res.msg);
                    $(location).attr('href', '/in/banners');
                }
            });
        }
    });

    $('#bannerSrc').on('change', function (e) {
        var uploadFile = this.files[0];
        if (!(/\.(jpg|png|gif)$/i).test(uploadFile.name)) {
            alert('El archivo a adjuntar no es una imagen');
        } else {
            var img = new Image();
            img.onload = function () {
                if (this.width.toFixed(0) != 1100 && this.height.toFixed(0) != 560) {
                    alert('Las medidas deben ser: 1100 * 560');
                    $('#bannerSrc').removeClass('border-green').addClass('border-red');
                    app.isvalid = false;
                } else {
                    $('#bannerSrc').removeClass('border-red').addClass('border-green');
                    app.isvalid = true;
                }
            };
            img.src = URL.createObjectURL(uploadFile);
            imgPreview(this, $('#is-preview'));
        }
    });

    $('.drop-banner').on('click', function (e) {
        e.preventDefault();
        if (confirm('¿Estás Seguro(a) de querer eliminar esta Imagen?')) {
            var img = $(this).data('img');
            var id = $(this).data('id');
            var data = {
                'bannerSrc': img,
                'idbanner': id,
                'action': 'delete'
            };
            $.ajax({
                data: data,
                url: app.path + 'handlers/hdl-banner.php',
                type: 'POST',
                success: function (data) {
                    var res = eval('(' + data + ')');
                    alert(res.msg);
                    location.reload();
                }
            });
        }
    });

    $('.ordn-banner').focusout(function (e) {
        e.preventDefault();
        var city = $(this).data('city');
        var id = $(this).data('id');
        var lOrd = $(this).data('order');
        var orden = $(this).val();
        $(this).addClass('valid-proces');
        if (orden != lOrd) {
            var data = {
                'bannerCity': city,
                'idbanner': id,
                'bannerOrden': orden,
                'action': 'order'
            };
            $.ajax({
                data: data,
                url: app.path + 'handlers/hdl-banner.php',
                type: 'POST',
                success: function (data) {
                    var res = eval('(' + data + ')');
                    alert(res.msg);
                    if (res.status == 'done') {
                        $('.valid-proces').removeClass('border-red').addClass('border-green');
                        location.reload();
                    } else {
                        $('.valid-proces').removeClass('border-green').addClass('border-red');
                        $('.ordn-banner').removeClass('valid-proces');
                    }
                }
            });
        } else {
            $(this).removeClass('border-red');
            $(this).removeClass('valid-proces');
        }
    });
});