<?php
$content = '
{
    "email": "[EMAIL]",
    "datetime": "[DATETIME]",
    "ip": "[IP]"
},';

if (isset($_POST['email']) && isset($_POST['from'])) {
    date_default_timezone_set('America/Cancun');
    
    require($_SERVER['DOCUMENT_ROOT'] . '/models/mMails.php');
    $mmails = new mMails();

    $datetime = date('Y-m-d H:i:s');
    $content = str_replace('[EMAIL]', $_POST['email'], $content);
    $content = str_replace('[DATETIME]', $datetime, $content);
    $content = str_replace('[IP]', $mmails->getIpClient(), $content);
    
    //PRINT INFO IN JSON
    $fp = fopen($_SERVER['DOCUMENT_ROOT'] . '/models/suscription.json', 'a');
    fwrite($fp, trim($content) . "\r\n");
    fclose($fp);
    
    //SEND CLIENT MAIL
    $bodyMail = $mmails->ClientTemplateHeader() . '<p style="font-family: '. "'Noto Sans', 'Helvetica'". ', Arial, sans-serif; color: #9444b3; font-size: 20px; text-align: center;">Gracias por suscribirte a <strong>Enlace TotalPlay</strong>.</p>' . $mmails->ClientTemplateFooter();

    $mmails->sendMail($_POST['email'], 'Enlace TotalPlay - Suscripción', $bodyMail);

    //SEND INFO MAIL
    $bodyMail = $mmails->templateHeader('Nueva Suscripción') . '<p style="font-family: '. "'Noto Sans', 'Helvetica'". ', Arial, sans-serif; color: #9444b3; text-align: center;"><strong>Correo Electrónico:</strong> '. $_POST['email'] .'</p>
    <p style="font-family: '. "'Noto Sans', 'Helvetica'". ', Arial, sans-serif; color: #9444b3; text-align: center;"><strong>Desde:</strong> '. $_POST['from'] .'</p>
    <p style="font-family: '. "'Noto Sans', 'Helvetica'". ', Arial, sans-serif; color: #9444b3; text-align: center;"><small>'. date('Y-m-d H:i:s') .'</small></p>' . $mmails->templateFooter();

    $mmails->sendMail('info@enlacetotalplay.com', 'Enlace TotalPlay - Nueva Suscripción', $bodyMail);

    echo true;
}