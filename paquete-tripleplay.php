<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view is-view-packages">
    <div class="container">
        <div class="columns is-multiline">

            <div class="column is-full">
                <h1 class="is-title-home">Paquete Triple<span class="cl-p">p</span><span class="cl-l">l</span><span class="cl-a">a</span><span class="cl-y">y</span></h1>

                <p class="is-text is-text-packages is-text-tripleplay is-show" id="is-text-tripleplay">Los siguientes planes incluyen un decodificador HD con punto de acceso WIFI1 que te permitirá acceder a tus aplicaciones favoritas</p>

            </div>

            <div class="column is-full is-packages">
                <div class="columns is-multiline">

                    <!--TRIPLEPLAY-->

                    <?php foreach ($packList['tripleplay'] as $tripleplay) { ?>
                        <div class="column is-one-quarter is-triple-pack">
                            <div class="is-item is-default <?= $tripleplay['color'] ?>">
                                <div class="is-title">
                                    <strong><?= $tripleplay['nombre'] ?></strong>
                                    <span class="is-megas is-size-4">Internet de <?= $tripleplay['megas'] ?><small>Megas</small></span>
                                </div>
                                <div class="is-complements is-clearfix is-flex-div">
                                    <span class="<?= ($tripleplay['canales'] == 0) ? 'is-hidden' : '' ?>">
                                        <i class="fas fa-tv"></i>
                                        <small><strong><?= $tripleplay['canales'] ?></strong> Canales</small>
                                        <small class="is-tag"><?= $tripleplay['tvs'] ?> TV</small>
                                    </span>
                                    <span class="<?= ($tripleplay['lineas'] == 0) ? 'is-hidden' : '' ?>">
                                        <i class="fas fa-fax"></i>
                                        <small><strong><?= $tripleplay['lineas'] ?></strong> Línea(s) telefónica(s)</small>
                                    </span>
                                </div>
                                <div class="is-discount">
                                    <strong class="is-size-4"><small>$</small><?= $tripleplay['descuentomxn'] ?><small class="is-size-7 dpy-inl">MXN</small><span class="dpy-inl is-size-6"> de Descuento</span></strong>
                                    <span class="is-size-6">a partir del <?= $tripleplay['mesdescuento'] ?>° MES</span>
                                </div>
                                <div class="is-total">
                                    <span>Pronto Pago</span>
                                    <strong>$<?= number_format($tripleplay['prontopago'], 0, '.', ',') ?><small class="dpy-inl">MXN</small></strong>
                                    <span>Precio de Lista</span>
                                    <strong>$<?= number_format($tripleplay['listapago'], 0, '.', ',') ?><small class="dpy-inl">MXN</small></strong>
                                    <a href="/contrata?paquete=<?= $tripleplay['cve'] ?>" class="btn-paquete">Contratar</a>
                                    <small>*Consulta términos y condiciones</small>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                </div>
            </div>

        </div>
    </div>
</section>

<?php require __DIR__ . '/components/footer.php'; ?>