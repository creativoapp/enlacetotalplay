<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view-home">
    
    
    <!--OVERVIEW-->
    <div class="container is-overview">
        <div class="columns">

            <div class="column is-half">
                <h1>Totalplay <?=$city_page['name']; ?> Cobertura</h1>
                <p>Totalplay es el proveedor de Internet por fibra óptica preferido <?=$city_page['name']; ?>. Destacados entre los mejores ISP del mercado, ofrecemos un servicio innovador. Cada vez más familias y empresas se suman a disfrutar de la mejor conectividad. Los paquetes de Totalplay más famosos son los paquetes Tripleplay, mismos que incluyen servicio de Internet, telefonía y TV.</p>
                <p>Calificados por nuestros clientes como el mejor <strong>proveedor de Internet empresarial</strong> y de uso personal, ofrecemos la mejor velocidad de Internet, los mejores programas de televisión digital, diversas aplicaciones para disfrutar y telefonía a bajo costo. Todo implementado a través de una potente conexión con fibra óptica.</p>
                <p>Nuestros paquetes Tripleplay son, sin duda, los mejores paquetes de Internet + TV + Telefonía en el mercado. Llegamos a revolucionar los servicios de telecomunicaciones haciendolos más eficientes. <span>¿Qué estás esperando? <a href="/totalplay-<?= $city_page['url']; ?>/contrata">¡Cámbiate ya!</a></span></p>
            </div>

            <div class="column is-half">
                <img src="/assets/img/totalplay-familiar.png" class="is-img-big is-img-centered">
            </div>

        </div>

        <div class="columns">
            <div class="column is-full">
                

            <div id="map" style="width:50%;height:400px;"></div>
    <div id="capture" style="width:50%;height:400px;"></div>
    <script>
      var map;
      var src = 'http://site.enlacetotalplay.com/aguascalientes.kmz';

      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: new google.maps.LatLng(-21.8857199, -102.3613401),
          zoom: 2,
          mapTypeId: 'roadmap'
        });

        var kmlLayer = new google.maps.KmlLayer(src, {
          suppressInfoWindows: true,
          preserveViewport: false,
          map: map
        });
        kmlLayer.addListener('click', function(event) {
          var content = event.featureData.infoWindowHtml;
          var testimonial = document.getElementById('capture');
          testimonial.innerHTML = content;
        });
      }
    </script>
    <script defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC5CP38GNHZgjL2f5hRP33hxGBWKvmMfPU&callback=initMap">
    </script>


                
            </div>
        </div>

    </div>

    <!--ADDITIONALS-->
    <div class="is-addons">
        <div class="container">
            <div class="columns is-variable is-5">

                <div class="column is-one-third">
                    <a href="/<?= $city_page['url']; ?>/empresarial"><img src="/assets/img/totalplay-empresarial.jpg"></a>
                    <h3><a href="/<?= $city_page['url']; ?>/empresarial">Para empresas</a></h3>
                    <p>Servicios de <strong>Internet empresarial</strong> y soluciones integrales para empresas, para que todo el equipo se mantenga conectado y productivo.</p>
                    <a href="/<?= $city_page['url']; ?>/empresarial" class="is-simple-link">Internet empresarial <i class="fas fa-arrow-right"></i></a>
                </div>

                <div class="column is-one-third">
                    <a href="/<?= $city_page['url']; ?>/promociones"><img src="/assets/img/totalplay-promociones.jpg"></a>
                    <h3><a href="/<?= $city_page['url']; ?>/promociones">Promociones</a></h3>
                    <p>Conoce nuestros descuentos, ofertas y promociones disponibles. Consulta términos y condiciones.</p>
                    <a href="/<?= $city_page['url']; ?>/promociones" class="is-simple-link">Promociones Totalplay <i class="fas fa-arrow-right"></i></a>
                </div>

                <div class="column is-one-third">
                    <a href="/<?= $city_page['url']; ?>/canales"><img src="/assets/img/totalplay-canales.jpg"></a>
                    <h3><a href="/<?= $city_page['url']; ?>/canales">Canales</a></h3>
                    <p>La mejor programación para que la diversión y el entretenimiento nunca se detengan. Conoce los canales incluídos en cada paquete de TV Totalplay.</p>
                    <a href="/<?= $city_page['url']; ?>/canales" class="is-simple-link">Canales incluidos <i class="fas fa-arrow-right"></i></a>
                </div>

            </div>
        </div>
    </div>

</section>

<?php require __DIR__ . '/components/footer.php'; ?>