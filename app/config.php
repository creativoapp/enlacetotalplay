<?php

//Constants
define('_IMG', '/assets/img/');
define('_CSS', '/assets/css/');
define('_JS', '/assets/js/');

//Global Vars
$routeUri = $_SERVER['PHP_SELF'];
$pageUri = basename($_SERVER['SCRIPT_NAME']);
$cityUriExtend = '';
$cv = '1.0.0';

$pageLayout = 'default';
switch($routeUri) {
    case '/city-page.php':
        $pageLayout = 'city';
        break;
    case '/city-internet-page.php':
        $pageLayout = 'city-internet-page';
        break;
    case '/city-tv-page.php':
        $pageLayout = 'city-tv-page';
        break;
    case '/city-phone-page.php':
        $pageLayout = 'city-phone-page';
        break;
    case '/city-package-page.php':
        $pageLayout = 'city-package-page';
        break;
    case '/city-deals-page.php':
        $pageLayout = 'city-deals-page';
        break;
    case '/city-business-page.php':
        $pageLayout = 'city-business-page';
        break;
    case '/city-contract-page.php':
        $pageLayout = 'city-contract-page';
        break;
    // case '/city-coverage-page.php':
    //     $pageLayout = 'city-coverage-page';
    //     break;
}

$monthList = [
    ['num' => '01', 'month' => 'Enero'],
    ['num' => '02', 'month' => 'Febrero'],
    ['num' => '03', 'month' => 'Marzo'],
    ['num' => '04', 'month' => 'Abril'],
    ['num' => '05', 'month' => 'Mayo'],
    ['num' => '06', 'month' => 'Junio'],
    ['num' => '07', 'month' => 'Julio'],
    ['num' => '08', 'month' => 'Agosto'],
    ['num' => '09', 'month' => 'Septiembre'],
    ['num' => '10', 'month' => 'Octubre'],
    ['num' => '11', 'month' => 'Noviembre'],
    ['num' => '12', 'month' => 'Diciembre'],
];

//LoadModels
require($_SERVER['DOCUMENT_ROOT'].'/models/mCities.php');
require($_SERVER['DOCUMENT_ROOT'].'/models/mSeo.php');

//City Rules
$citiesObj = new CitiesModel();
$cities = $citiesObj->citiesList('mx');
$city_page = array('url' => null, 'name' => null, 'nav' => '');

// City Page
if($pageLayout == 'city') {
    $city_arg = explode('=', $_SERVER['QUERY_STRING']);
    $city_page['url'] = $city_arg[1];

    if(isset($cities->{''.$city_arg[1].''})) {
        $city_page['name'] = $cities->{''.$city_arg[1].''}->{'text'};
        $cityUriExtend = 'totalplay-'.$cities->{''.$city_arg[1].''}->{'url'};
    }

}

// City -> {Section}
if($pageLayout != 'default' && $pageLayout != 'city') {
    $city_arg = explode('=', $_SERVER['QUERY_STRING']);
    $city_url = explode('/', $city_arg[1]);
    $city_page['url'] = $city_url[0];

    if(isset($cities->{''.$city_url[0].''})) {
        $city_page['name'] = $cities->{''.$city_url[0].''}->{'text'};
        $cityUriExtend = 'totalplay-'.$cities->{''.$city_url[0].''}->{'url'};
    }
}

//Seo Rules
$seoObj = new SeoModel();

//Banner
if ($routeUri == '/index.php' || $routeUri == '/city-page.php') {
    require('models/mBanner.php');
    $mBanner = new Banner();
    $fCiudad = null;
    if (isset($_GET['city'])) {
        $fCiudad = $_GET['city'];
    }
    $bannerList = $mBanner->getBanner($fCiudad);
}

$packages = array('/paquetes.php',
                 '/paquetes-dobleplay.php',
                 '/paquete-tripleplay.php',
                 '/city-package-page.php',
                 '/contrata.php',
                 '/city-contract-page.php');

//if ($routeUri == '/paquetes.php' || $routeUri == '/city-package-page.php' || $routeUri == '/contrata.php' || $routeUri == '/city-contract-page.php') {
if(in_array($routeUri, $packages)) {
    require('models/mPaquetes.php');
    $mPaquetes = new Paquetes();
    $packList = $mPaquetes->getPaquetes();
    
}

if ($routeUri == '/city-contract-page.php') {
    $destroy = explode('=', $_SERVER['REQUEST_URI']);
    if (isset($destroy[1])) {
        $_GET['paquete'] = $destroy[1];
    }
}

if ($routeUri == '/promociones.php' || $routeUri == '/city-deals-page.php') {
    require('models/mPromociones.php');
    $mPromociones = new Promociones();
    $fCiudad = null;
    if (isset($_GET['city'])) {
        $fCiudad = str_replace('/promociones', '', $_GET['city']);
    }
    $promList = $mPromociones->getPromociones($fCiudad);
    $month = $monthList[date('m')-1]['month'];
}

//Cobertura
if($routeUri == '/city-coverage-page.php') {
    $city_arg = explode('=', $_SERVER['QUERY_STRING']);
    $city_url = explode('/', $city_arg[1]);
    $city_page['url'] = $city_url[0];

    if(isset($cities->{''.$city_url[0].''})) {
        $city_page['name'] = $cities->{''.$city_url[0].''}->{'text'};
        $cityUriExtend = 'totalplay-'.$cities->{''.$city_url[0].''}->{'url'};
    }
}