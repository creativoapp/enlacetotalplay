<?php
include('../components/header.php');
$label = 'Agregar';
$src = null;
if (isset($_GET['action']) && $_GET['action'] == 'edit') {
    $label = 'Modificar';
    if (isset($_GET['cve'])) {
        $mPromociones = new Promociones();
        $promocion = $mPromociones->getPromocion($_GET['cve']);
        $src = '/assets/img/promociones/' . $promocion['promocionImg'];
    }
}
?>
<div class="container is-view">
    <div class="columns">
        <div class="column">
            <h1 class="is-size-4"><?= $label ?> Promoción</h1>
            <nav class="breadcrumb" aria-label="breadcrumbs">
                <ul>
                    <li><a href="/in/promociones">Promociones</a></li>
                    <li class="is-active"><a aria-current="page"><?= $label ?> Promoción</a></li>
                </ul>
            </nav>
        </div>
    </div>
    <div class="columns">
        <div class="column is-two-thirds">
            <form action="#" id="frm-promocion">
                <fieldset class="f-md">
                    <label for="">Imagen * (780 x 520)</label>
                    <input type="file" name="promocionImg" id="promocionImg" accept="image/*">
                    <input type="hidden" id="promocionImg-name" name="promocionImg-name" value="<?= (isset($promocion['promocionImg']) ? $promocion['promocionImg'] : '') ?>">
                </fieldset>
                <fieldset class="f-md">
                    <label for="">Ciudad</label>
                    <select name="promocionCity" id="promocionCity">
                        <option value="">General</option>
                        <?php
                        $cityUriExtend = '';
                        if (isset($promocion['promocionCity'])) {
                            $cityUriExtend = $promocion['promocionCity'];
                        }
                        foreach ($cities as $key => $city) {
                            $selected = '';
                            if ($key == $cityUriExtend) {
                                $selected = 'selected';
                            }
                            echo '<option value="' . $city->url . '" ' . $selected . ' >' . $city->text . '</option>' . "\r\n";
                        }
                        ?>
                    </select>
                    <input type="hidden" id="last-city" name="last-city" value="<?= $cityUriExtend ?>">
                </fieldset>
                <div class="clr"></div>
                <fieldset class="f-md">
                    <label for="">Nombre * <small class="msg-name has-text-danger"></small></label>
                    <input type="text" id="shortName" name="shortName" maxlength="100" placeholder="Nombre Corto" value="<?= (isset($promocion['shortName']) ? $promocion['shortName'] : '') ?>">
                </fieldset>
                <fieldset class="f-md">
                    <label for="">Titulo *</label>
                    <input type="text" id="promocionTitulo" name="promocionTitulo" maxlength="250" placeholder="Titulo" value="<?= (isset($promocion['promocionTitulo']) ? $promocion['promocionTitulo'] : '') ?>">
                </fieldset>
                <div class="clr"></div>
                <fieldset class="f-md">
                    <label for="">Identificador * <small class="msg-cve has-text-danger"></small></label>
                    <input type="text" id="promocioncve" name="promocioncve" class="text-cve" maxlength="200" placeholder="identificador" value="<?= (isset($promocion['promocioncve']) ? $promocion['promocioncve'] : '') ?>">
                </fieldset>
                <fieldset class="f-md">
                    <label for="">Vigencia *</label>
                    <input type="date" id="promocionVigencia" name="promocionVigencia" maxlength="250" placeholder="" value="<?= (isset($promocion['promocionVigencia']) ? $promocion['promocionVigencia'] : '') ?>">
                </fieldset>
                <div class="clr"></div>
                <fieldset>
                    <label for="">Descripción</label>
                    <textarea name="promocionDescription" id="promocionDescription" cols="30" rows="6"><?= (isset($promocion['promocionDescription']) ? $promocion['promocionDescription'] : '') ?></textarea>
                </fieldset>
                <fieldset>
                    <input type="hidden" name="idpromocion" id="idpromocion" value="<?= (isset($promocion['idpromocion']) ? $promocion['idpromocion'] : '') ?>">
                    <input type="hidden" name="action" id="action" value="<?= ((isset($_GET['action']) && $_GET['action'] == 'edit') ? $_GET['action'] : 'create') ?>">
                    <input type="submit" value="<?= $label ?>">
                </fieldset>
            </form>
        </div>
        <div class="column">
            <img id="is-preview" class="is-img-full" src="<?= $src ?>" alt="">
        </div>
    </div>
</div>
<?php include('../components/footer.php'); ?>