<form name="formGlobalService" method="post" action="" class="columns is-multiline">
    <fieldset class="column is-full">
        <label for="inpName">Nombre</label>
        <input type="text" name="inpName" id="inpName">
    </fieldset>
    <fieldset class="column is-half">
        <label for="inpEmail">Email</label>
        <input type="text" name="inpEmail" id="inpEmail">
    </fieldset>
    <fieldset class="column is-half">
        <label for="inpNumber">Teléfono</label>
        <input type="text" name="inpNumber" id="inpNumber">
    </fieldset>
    <fieldset class="column is-full">
        <label for="inpComments">Estoy interesado en</label>
        <textarea name="inpComments" id="inpComments" rows="5"></textarea>
    </fieldset>
    <fieldset class="column is-full">
        <button id="btnSendGlobalForm">CONTACTAR <i class="fas fa-arrow-right"></i></button>
    </fieldset>
</form>