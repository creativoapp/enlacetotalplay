<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view is-view-service">

    <!--OVERVIEW-->
    <div class="container">
        <div class="columns">
            
            <div class="column is-half is-overview">
                <h1 class="is-title-home">TV Total<span class="cl-p">p</span><span class="cl-l">l</span><span class="cl-a">a</span><span class="cl-y">y</span></h1>
                <p class="is-pr-big">Programación para toda la familia.</p>
                <p class="has-text-justified">Desde el plan más básico de TV, tendrás incluidos canales para todos los gustos. Consulta la <a href="/canales">sección de canales</a> para ver la lista completa de canales de cada plan.</p>
                <p class="has-text-justified">Para conocer cuántas TV incluye cada paquete, visita nuestra <a href="/paquetes">sección de paquetes.</a></p>
            
                <div class="is-clearfix">
                    <div class="is-item is-item-tv">
                        <i class="fas fa-check-circle"></i>
                        <h2 class="is-size-6 dpy-inl">Canales en 4K</h2>
                        <p>¡No te pierdas ni un detalle! Programación con definición de 4k. </p>
                    </div>
                    <div class="is-item is-item-tv">
                        <i class="fas fa-check-circle"></i>
                        <h2 class="is-size-6 dpy-inl">Deportes Total</h2>
                        <p>Disfruta de los mejores partidos y encuentros gracias a los canales deportivos incluidos en los paquetes. Porque más que una afición, es algo que te define.</p>
                    </div>
                    <div class="is-item is-item-tv">
                        <i class="fas fa-check-circle"></i>
                        <h2 class="is-size-6 dpy-inl">Peliculas OnDemmand</h2>
                        <p>Disfruta de películas clásicas y taquilleras.</p>
                    </div>
                </div>

            </div>

            <div class="column is-half is-overview dv-center-content">
                <img src="/assets/img/television-totalplay.png" class="is-img-centered">
            </div>

        </div>
    </div>

    <div class="is-row-service">
        <div class="container">
            <div class="columns is-multiline">
            
                <div class="column is-one-third is-item is-item-tv">
                    <h3><i class="fas fa-map-marker-alt"></i>Programación remota</h3>
                    <p>Usa la app para ver tus programas favoritos en donde estés.</p>
                </div>

                <div class="column is-one-thid is-item is-item-tv">
                    <h3><i class="fas fa-tv"></i>Alta Definición</h3>
                    <p>Programación en HD sin costo adicional en todos nuestros planes y sin contratar equipos extras. La TV como siempre la quisiste ver.</p>
                </div>

                <div class="column is-one-third is-item is-item-tv">
                    <h3><i class="fas fa-laptop"></i>Video On Demand</h3>
                    <p>Cientos de títulos entre películas, telenovelas, series, conciertos, programación infantil y mucho más.</p>
                </div>

                <div class="column is-one-third is-item is-item-tv">
                    <h3><i class="fas fa-vr-cardboard"></i>Películas en 3D</h3>
                    <p>Consigue tus lentes 3D, unas palomitas y vive la experiencia de las películas en tercera dimensión en la comodidad de tu hogar.</p>
                </div>

                <div class="column is-one-third is-item is-item-tv">
                    <h3><i class="fas fa-mobile-alt"></i>Controla con tu Smartphone</h3>
                    <p>Por si no consigues el control, siempre tendrás la opción de cambiar de canal, ajustar el volumen y controlar tu TV desde tu celular a través de la app de Totalplay.</p>
                </div>

            </div>
        </div>
    </div>

</section>

<?php require __DIR__ . '/components/footer.php'; ?>