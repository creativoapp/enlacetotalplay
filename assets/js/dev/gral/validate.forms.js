$(document).ready(function(){
    $('a#submit-contact').on('click', function(e){
        e.preventDefault();
        $('#frmServiceDetail').submit();
    });
    $('#frmServiceDetail').on('submit', function(){
        var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
        if ($('#inpName').val().trim() == '') {
            $('#inpName').focus();
            return false;
        } else if ($('#inpEmail').val().trim() == '' || regex.test($('#inpEmail').val().trim()) === false) {
            $('#inpEmail').focus();
            return false;
        } else if ($('#inpNumberPhone').val().trim() == '' || $('#inpNumberPhone').val().trim().length < 10) {
            $('#inpNumberPhone').focus();
            return false;
        } else if ($('#inpSubject').length == 0 && $('#inpServiceInteresting').val().trim() == 'Any' && $('#inpComments').val().trim() == '') {
            $('#inpComments').focus();
            return false;
        } else if ($('#inpSubject').length > 0 && $('#inpSubject').val().trim() == '') {
            $('#inpSubject').focus();
            return false;
        } else {
            return true;
        }
    });

    //CONTACT FORM
    $('form#contact-form').on('submit', function(e){
        e.preventDefault();
        var name = $('#cp-cnc-name').val().trim();
        var email = $('#cp-cnc-email').val().trim();
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var from = new String(window.location);
        var valid = true;
        if (email == '' || !regex.test(email)) {
            valid = false;
            $('#cp-cnc-email').focus();
        }
        if (name == '') {
            valid = false;
            $('#cp-cnc-name').focus();
        }
        if (valid == true) {
            var data = {
                'name': name,
                'email': email,
                'number': $('#cp-cnc-phone').val().trim(),
                'messaje': $('#cp-cnc-message').val().trim(),
                'promo': $('#promo').val().trim(),
                'from-promo': $('#from-promo').val().trim(),
                'from': from,
                'type': 'contact'
            }
            $.ajax({
                data: data,
                url: window.location.protocol + '//' + window.location.hostname + '/handlers/hdl-mails.php',
                type: 'POST',
                beforeSend: function () {
                    $('#cp-cnc-button').val('ENVIANDO...');
                    $('#cp-cnc-button').attr("disabled", true);
                },
                success: function (rs) {
                    if (rs == true) {
                        alert('Gracias por contactarte con nosotros!!');
                        location.reload();
                    }
                    if (rs == false) {
                        alert('Oh oh, no se han podido enviar los datos.'+"\r\nIntentelo mas tarde por favor!!");
                        $('#cp-cnc-button').val('QUIERO QUE ME CONTACTEN');
                        $('#cp-cnc-button').attr("disabled", false);
                    }
                }
            });
        }
    });

    //SIMPLE CONTACT
    $('form#simple-contact').on('submit', function(e){
        e.preventDefault();
        var name = $('#cp-call-name').val().trim();
        var email = $('#cp-call-email').val().trim();
        var codigo = $('#cp-call-zip').val().trim();
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var from = new String(window.location);
        var valid = true;
        if (codigo == '') {
            valid = false;
            $('#cp-call-zip').focus();
        }
        if (email == '' || !regex.test(email)) {
            valid = false;
            $('#cp-call-email').focus();
        }
        if (name == '') {
            valid = false;
            $('#cp-call-name').focus();
        }
        if (valid == true) {
            var data = {
                'name': name,
                'email': email,
                'codigo': codigo,
                'number': $('#cp-call-phone').val().trim(),
                'from': from,
                'type': 'simple-contact'
            }
            $.ajax({
                data: data,
                url: window.location.protocol + '//' + window.location.hostname + '/handlers/hdl-mails.php',
                type: 'POST',
                beforeSend: function () {
                    $('#cp-call-button').val('ENVIANDO...');
                    $('#cp-call-button').attr("disabled", true);
                },
                success: function (rs) {
                    if (rs == true) {
                        alert('Gracias por contactarte con nosotros!!');
                        location.reload();
                    }
                    if (rs == false) {
                        alert('Oh oh, no se han podido enviar los datos.'+"\r\nIntentelo mas tarde por favor!!");
                        $('#cp-call-button').val('QUIERO QUE ME CONTACTEN');
                        $('#cp-call-button').attr("disabled", false);
                    }
                }
            });
        }
    });

    //CONTRATAR
    $('form#contrata-form').on('submit', function(e){
        e.preventDefault();
        var name = $('#cp-cnt-name').val().trim();
        var email = $('#cp-cnt-email').val().trim();
        var city = $('#cp-cnt-city').val().trim();
        var phone = $('#cp-cnt-phone').val().trim();
        var codigo = $('#cp-cnt-cpostal').val().trim();
        var interesting = $('#cp-cnt-interesting').val().trim();
        var medio = $('#cp-cnt-medio').val().trim();
        var otro = $('#cp-cnt-medio-other').val().trim();
        var message = $('#cp-cnt-message').val().trim();
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var valid = true;
        var from = new String(window.location);
        if (medio == '0') {
            $('#cp-cnt-medio').focus();
            valid = false;
        }
        if (medio == 'Otro' && otro == '') {
            $('#cp-cnt-medio-other').focus();
            valid = false;
        }
        if (interesting == '0') {
            $('#cp-cnt-interesting').focus();
            valid = false;
        }
        if (codigo == '') {
            $('#cp-cnt-cpostal').focus();
            valid = false;
        }
        if (city == '0') {
            $('#cp-cnt-city').focus();
            valid = false;
        }
        if (email == '' || regex.test(email) == false) {
            $('#cp-cnt-email').focus();
            valid = false;
        }
        if (name == '') {
            $('#cp-cnt-name').focus();
            valid = false;
        }
        if (valid == true) {
            var data = {
                'name': name,
                'email': email,
                'codigo': codigo,
                'number': phone,
                'interesting': interesting,
                'city': city,
                'medio': medio,
                'otro': otro,
                'message': message,
                'from': from,
                'type': 'contrata-form'
            };
            $.ajax({
                data: data,
                url: window.location.protocol + '//' + window.location.hostname + '/handlers/hdl-mails.php',
                type: 'POST',
                beforeSend: function () {
                    $('#cp-cnt-button').val('ENVIANDO...');
                    $('#cp-cnt-button').attr("disabled", true);
                },
                success: function (rs) {
                    if (rs == true) {
                        alert('Gracias por contactarte con nosotros.'+"\r\nNos pondremos en contacto con tigo.");
                        location.reload();
                    }
                    if (rs == false) {
                        alert('Oh oh, no se han podido enviar los datos.'+"\r\nIntentelo mas tarde por favor!!");
                        $('#cp-cnt-button').val('QUIERO CONTRATAR');
                        $('#cp-cnt-button').attr("disabled", false);
                    }
                }
            });
        }
    });

    $('#cp-cnt-medio').on('change', function(){
        var value = $(this).val();
        if (value == 'Otro') {
            $('input#cp-cnt-medio-other').css('display', 'block');
            $('input#cp-cnt-medio-other').focus();
        } else {
            $('input#cp-cnt-medio-other').css('display', 'none');
        }
    });

    $('input#cp-call-zip, input#cp-cnt-cpostal').on('input', function(){
        var replace = $(this).val();
        replace = replace.replace(/[^0-9. ]/g, '');
        $(this).val(replace);
    });

    $('input#cp-call-phone, input#cp-cnc-phone, input#cp-cnt-phone').on('input', function(){
        var replace = $(this).val();
        replace = replace.replace(/[^0-9+ ()-]/g, '');
        $(this).val(replace);
    });
});